<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /*RENDERIZADO DE LA VISTA LISTA*/
    public function actionLista()
    {
        return $this->render('lista');
    }
    /*RENDERIZADO DE LA VISTA DE POSIBLES CASOS DE USO*/
    public function actionFunciones()
    {
        return $this->render('funciones');
    }
    
    /* CONSULTAS*/
    
    /* CONSULTAS DE LOS PEDIDOS*/
        /* MUESTRA DE TODOS LOS PEDIDOS REGISTRADOS*/
            /* CONSULTA CON DAO*/
    public function actionConsulta1a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT * FROM pedidos',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta1a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON DAO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos"
            
        ]);
        
                
        
    }
    

    
    /*MUESTRA ÚNICAMENTE DE LOS ENVÍOS REGISTRADOS*/
        /*CONSULTA CON DAO*/
    public function actionConsulta2a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where tipo_pedido="E"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT * FROM pedidos  WHERE tipo_pedido="E"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta2a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS ENVÍOS REGISTRADOS CON DAO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos  WHERE tipo_pedido='E'"
            
        ]);
}

 /*MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS*/
        /*CONSULTA CON DAO*/
    public function actionConsulta3a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where tipo_pedido="R"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT * FROM pedidos  WHERE tipo_pedido="R"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta3a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODAS LAS RECEPCIONES REGISTRADAS CON DAO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos  WHERE tipo_pedido='R'"
            
        ]);
}


/*MUESTRA ÚNICAMENTE DE LOS PEDIDOS FACILITADOS POR PROVEEDORES*/
        /*CONSULTA CON DAO*/
    public function actionConsulta4a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos inner join proveedores on pedidos.codigo_proveedor=proveedores.codigo_proveedor')
                ->queryScalar();
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT * FROM pedidos INNER JOIN proveedores ON pedidos.codigo_proveedor=proveedores.codigo_proveedor',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
        ]);
      
        return $this-> render ("consulta4a",[
            "resultados"=> $dataProvider,
            "campos"=>['nombre_proveedor', 'tracking_pedido','carpeta_pedido','estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS FACILITADOS POR PROVEEDORES CON DAO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos INNER JOIN proveedores ON pedidos.codigo_proveedor=proveedores.codigo_proveedor"           
        ]);
    }

/*MUESTRA ÚNICAMENTE DE LOS PEDIDOS ENCARGADOS POR CLIENTES*/
/* CONSULTA CON DAO*/
 public function actionConsulta5a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos inner join clientes on pedidos.codigo_cliente=clientes.codigo_cliente')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT * FROM pedidos INNER JOIN clientes ON pedidos.codigo_cliente = clientes.codigo_cliente ',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta5a",[
            "resultados"=> $dataProvider,
            "campos"=>['nombre_cliente','tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS ENCARGADOS POR CLIENTES",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos  INNER JOIN clientes ON pedidos.codigo_cliente = clientes.codigo_cliente"
            
        ]);
}

/*CONSULTAS DE AGRUPACIÓN DE LOS PEDIDOS POR TIPO DE ESTADO QUE TIENEN*/
/*MUESTRA DE LOS PEDIDOS CON ESTADO PREVIO A PROCESO*/
/* CONSULTA CON DAO*/
 public function actionConsulta6a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="PREVIO A PROCESO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="PREVIO A PROCESO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta6a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PREVIO A PROCESO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='PREVIO A PROCESO'"
            
        ]);
}
/*MUESTRA DE LOS PEDIDOS CON ESTADO NUEVA ORDEN */
/* CONSULTA CON DAO*/
 public function actionConsulta7a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="NUEVA ORDEN"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="NUEVA ORDEN"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta7a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO NUEVA ORDEN",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='NUEVA ORDEN'"
            
        ]);
}

/*MUESTRA DE LOS PEDIDOS CON ESTADO ACEPTADA */
/* CONSULTA CON DAO*/
 public function actionConsulta8a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="ACEPTADA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="ACEPTADA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta8a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO ACEPTADA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='ACEPTADA'"
            
        ]);
}

/*MUESTRA DE LOS PEDIDOS CON ESTADO EN PROCESO */
/* CONSULTA CON DAO*/
 public function actionConsulta9a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="EN PROCESO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="EN PROCESO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta9a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO EN PROCESO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='EN PROCESO'"
            
        ]);
}

/*MUESTRA DE LOS PEDIDOS CON ESTADO PENDIENTE DE PAGO */
/* CONSULTA CON DAO*/
 public function actionConsulta10a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="PENDIENTE DE PAGO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="PENDIENTE DE PAGO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta10a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PENDIENTE DE PAGO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='PENDIENTE DE PAGO'"
            
        ]);
}

/*MUESTRA DE LOS PEDIDOS CON ESTADO ENVIADO */
/* CONSULTA CON DAO*/
 public function actionConsulta11a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="ENVIADO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="ENVIADO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta11a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO ENVIADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='ENVIADO'"
            
        ]);
}


/*MUESTRA DE LOS PEDIDOS CON ESTADO RECIBIDO */
/* CONSULTA CON DAO*/
 public function actionConsulta12a (){
        $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="RECIBIDO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="RECIBIDO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta12a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO RECIBIDO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='RECIBIDO'"
            
        ]);
}
public function actionConsulta13a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="PDTE FACTURA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="PDTE FACTURA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta13a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PDTE FACTURA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='PDTE FACTURA'"
            
        ]);
    
    
}

public function actionConsulta14a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="COMPLETADO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="COMPLETADO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta14a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO COMPLETADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='COMPLETADO'"
            
        ]);
    
    
}

public function actionConsulta15a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="RECIBIDO DE FORMA PARCIAL"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="RECIBIDO DE FORMA PARCIAL"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta15a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO RECIBIDO DE FORMA PARCIAL",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='RECIBIDO DE FORMA PARCIAL'"
            
        ]);
    
    
}

public function actionConsulta16a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where estado_pedido="INCIDENCIA DE ENVIO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE estado_pedido="INCIDENCIA DE ENVIO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta16a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO INCIDENCIA DE ENVIO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE estado_pedido='INCIDENCIA DE ENVIO'"
            
        ]);
    
    
}

public function actionConsulta17a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where urgencia_pedido="PRIORIDAD ALTA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE urgencia_pedido="PRIORIDAD ALTA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta17a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD ALTA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE urgencia_pedido='PRORIDAD ALTA'"
            
        ]);
    
    
}

public function actionConsulta18a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where urgencia_pedido="PRIORIDAD MEDIA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE urgencia_pedido="PRIORIDAD MEDIA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta18a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD MEDIA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE urgencia_pedido='PRORIDAD MEDIA'"
            
        ]);
    
    
}

public function actionConsulta19a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where urgencia_pedido="PRIORIDAD BAJA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE urgencia_pedido="PRIORIDAD BAJA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta19a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD BAJA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE urgencia_pedido='PRORIDAD BAJA'"
            
        ]);
    
    
}

public function actionConsulta20a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="TRANSFERENCIA"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="TRANSFERENCIA"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta20a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO TRANSFERENCIA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TRANSFERENCIA'"
            
        ]);
    
    
}

public function actionConsulta21a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="DOMICILIADO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="DOMICILIADO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta21a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO DOMICILIADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='DOMICILIADO'"
            
        ]);
    
    
}

public function actionConsulta22a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="PAYPAL"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="PAYPAL"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta22a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO PAYPAL",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='PAYPAL'"
            
        ]);
    
    
}

public function actionConsulta23a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="TARJETA ****9290"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="TARJETA ****9290"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta23a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO TARJETA ****9290",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TARJETA ****9290'"
            
        ]);
    
    
}

public function actionConsulta24a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="TARJETA ****4504"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="TARJETA ****4504"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta24a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO TARJETA ****4504",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TARJETA ****4504'"
            
        ]);
    
    
}


public function actionConsulta25a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where forma_de_pago="EFECTIVO"')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE forma_de_pago="EFECTIVO"',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta25a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO EFECTIVO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='EFECTIVO'"
            
        ]);
    
    
}


public function actionConsulta26a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos where codigo_proveedor is null and codigo_cliente is null')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos WHERE codigo_proveedor IS NULL AND codigo_cliente IS null',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta26a",[
            "resultados"=> $dataProvider,
            "campos"=>['tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS NO RELACIONADOS CON CLIENTES O PROVEEDORES",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos WHERE codigo_proveedor IS NULL AND codigo_cliente IS null"
            
        ]);
    
    
}

public function actionConsulta27a(){
    
     $numero = Yii::$app->db
                ->createCommand('select * from pedidos group by fecha_encargo')
                ->queryScalar();
        
        $dataProvider= new \yii\data\SqlDataProvider([
            'sql'=> ' SELECT * FROM pedidos group by fecha_encargo',
            'totalCount'=> $numero,
            /*DEPENDIENDO DE LA PAGINACIÓN, APARECERA UNA CANTIDAD
             * DE REGISTROS U OTRA
             */
            'pagination'=> [
                'pageSize'=>100,
                ]
                
            
        ]);
        
        return $this-> render( "consulta27a",[
            "resultados"=> $dataProvider,
            "campos"=>['fecha_encargo','tracking_pedido','carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS AGRUPADOS POR FECHA DE ENCARGO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO DAO",
            "sql"=> "SELECT * FROM pedidos group by fecha_encargo"
            
        ]);
    
    
}


 }

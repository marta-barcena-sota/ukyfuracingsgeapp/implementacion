<?php

namespace app\controllers;

use Yii;
use app\models\Pedidos;
use app\models\PedidosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * PedidosController implements the CRUD actions for Pedidos model.
 */
class PedidosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pedidos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PedidosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pedidos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pedidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pedidos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_pedido]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pedidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_pedido]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pedidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pedidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pedidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pedidos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
      /* CONSULTAS*/
//    
//    /* CONSULTAS DE LOS PEDIDOS*/
//        /* MUESTRA DE TODOS LOS PEDIDOS REGISTRADOS*/
//        /*CONSULTA CON ORM*/
 public function actionConsulta1b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido"),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta1b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ORM",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos",
            
        ]);
}
// /*MUESTRA ÚNICAMENTE DE LOS ENVÍOS REGISTRADOS*/
// /*CONSULTA CON ORM*/
  public function actionConsulta2b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
                -> where('tipo_pedido="E"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta2b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS ENVÍOS REGISTRADOS CON ORM",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE tipo_pedido='E'",
            
        ]);
 }
//
///*MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS*/
// /*CONSULTA CON ORM*/
  public function actionConsulta3b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
                -> where('tipo_pedido="R"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta3b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS CON ORM",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE tipo_pedido='R'",
            
        ]);
 } 
//    
///*MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS*/
// /*CONSULTA CON ORM*/
  public function actionConsulta4b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select(" tracking_pedido, carpeta_pedido, "
                    . "estado_pedido, urgencia_pedido, nombre_proveedor")
               ->join('INNER JOIN', 'proveedores', 'pedidos.codigo_proveedor=proveedores.codigo_proveedor'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta4b",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre_proveedor','tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS CON ORM",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT  tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido, 
                nombre_proveedor FROM pedidos INNER JOIN 
                proveedores p ON pedidos.codigo_proveedor = p.codigo_proveedor;
",
            
        ]);
 } 
//      
// /*MUESTRA ÚNICAMENTE DE LOS PEDIDOS ENCARGADOS POR CLIENTES*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta5b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select(" pedidos.codigo_cliente,tracking_pedido, carpeta_pedido, "
                    . "estado_pedido, urgencia_pedido, nombre_cliente")
               ->join('INNER JOIN', 'clientes', 'pedidos.codigo_cliente=clientes.codigo_cliente'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta5b",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre_cliente','tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA ÚNICAMENTE DE LAS RECEPCIONES REGISTRADAS CON ORM",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT tracking_pedido, carpeta_pedido, 
                estado_pedido, urgencia_pedido, nombre_cliente FROM pedidos 
                INNER JOIN clientes c ON pedidos.codigo_cliente = c.codigo_cliente;
",
            
        ]);
 } 
// 
// /*AGRUPACIÓN DE LOS PEDIDOS SEGÚN EL ESTADO EN EL QUE SE ENCUENTRAN*/
// 
// 
///*MUESTRA DE LOS PEDIDOS CON ESTADO PREVIO A PROCESO*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta6b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="PREVIO A PROCESO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta6b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PREVIO A PROCESO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='PREVIO A PROCESO'",
            
        ]);
 } 
//
// /*MUESTRA DE LOS PEDIDOS CON ESTADO NUEVA ORDEN*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta7b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="NUEVA ORDEN"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta7b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO NUEVA ORDEN",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='NUEVA ORDEN'",
            
        ]);
 } 
// 
// /*MUESTRA DE LOS PEDIDOS CON ESTADO ACEPTADA*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta8b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="ACEPTADA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta8b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO ACEPTADA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='ACEPTADA'",
            
        ]);
 } 
// 
// /*MUESTRA DE LOS PEDIDOS CON ESTADO EN PROCESO*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta9b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="EN PROCESO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta9b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO EN PROCESO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='EN PROCESO'",
            
        ]);
 } 
//
//  /*MUESTRA DE LOS PEDIDOS CON ESTADO PENDIENTE DE PAGO*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta10b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="PENDIENTE DE PAGO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta10b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PENDIENTE DE PAGO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='PENDIENTE DE PAGO'",
            
        ]);
 }
// 
// /*MUESTRA DE LOS PEDIDOS CON ESTADO ENVIADO*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta11b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="ENVIADO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta11b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO ENVIADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='ENVIADO'",
            
        ]);
 }
// /*MUESTRA DE LOS PEDIDOS CON ESTADO RECIBIDO*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta12b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="RECIBIDO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta12b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO RECIBIDO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='RECIBIDO'",
            
        ]);
 }
//  
//  /*MUESTRA DE LOS PEDIDOS CON ESTADO PDTE FACTURA*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta13b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="PDTE FACTURA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta13b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO PDTE FACTURA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='PDTE FACTURA'",
            
        ]);
 }
//
//   /*MUESTRA DE LOS PEDIDOS CON ESTADO PDTE FACTURA*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta14b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="COMPLETADO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta14b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO COMPLETADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='COMPLETADO'",
            
        ]);
 }
//  
//    /*MUESTRA DE LOS PEDIDOS CON ESTADO PDTE FACTURA*/
//  /*CONSULTA CON ORM*/
  public function actionConsulta15b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="RECIBIDO DE FORMA PARCIAL"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta15b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO RECIBIDO DE FORMA PARCIAL",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='RECIBIDO DE FORMA PARCIAL'",
            
        ]);
 }
//  
   public function actionConsulta16b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido, urgencia_pedido")
               ->WHERE('estado_pedido="INCIDENCIA DE ENVÍO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta16b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido', 'urgencia_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON ESTADO INCIDENCIA DE ENVÍO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE estado_pedido='INCIDENCIA DE ENVÍO'",
            
        ]);
 }
 
  public function actionConsulta17b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('urgencia_pedido="PRIORIDAD ALTA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta17b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD ALTA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE urgencia_pedido='PRIORIDAD ALTA'",
            
        ]);
 }
  
 
  public function actionConsulta18b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('urgencia_pedido="PRIORIDAD MEDIA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta18b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD MEDIA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE urgencia_pedido='PRIORIDAD MEDIA'",
            
        ]);
 }
   
   public function actionConsulta19b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('urgencia_pedido="PRIORIDAD BAJA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta19b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON URGENCIA PRIORIDAD BAJA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "  SELECT * FROM pedidos WHERE urgencia_pedido='PRIORIDAD BAJA'",
            
        ]);
 }
  
 public function actionConsulta20b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="TRANSFERENCIA"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta20b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO TRANSFERENCIA",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TRANSFERENCIA'",
            
        ]);
 } 
 
  public function actionConsulta21b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="DOMICILIADO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta21b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO DOMICILIADO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='DOMICILIADO'",
            
        ]);
 } 
 
  public function actionConsulta22b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="PAYPAL"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta22b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO PAYPAL",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='PAYPAL'",
            
        ]);
 } 
 
   public function actionConsulta23b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="TARJETA ****9290"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta23b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO TARJETA ****9290",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TARJETA ****9290'",
            
        ]);
 } 

   public function actionConsulta24b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="TARJETA ****4504"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta24b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE TARJETA ****4504",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='TARJETA ****4504'",
            
        ]);
 } 
  
   public function actionConsulta25b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('forma_de_pago="EFECTIVO"'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta25b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS CON FORMA DE PAGO EFECTIVO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE forma_de_pago='EFECTIVO'",
            
        ]);
 } 
  
 public function actionConsulta26b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->WHERE('codigo_proveedor= null')->where('codigo_cliente=null'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta26b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS NO RELACIONADOS CON CLIENTES O PROVEEDORES",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos WHERE codigo_proveedor IS NULL AND codigo_cliente IS null",
            
        ]);
 } 
 
 
 public function actionConsulta27b (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("tracking_pedido, carpeta_pedido, estado_pedido")
               ->groupBy('fecha_encargo'),
            'pagination'=>[
                'pageSize'=>100,
            ]
        ]);
        
        return $this-> render("consulta26b",[
            "resultados"=>$dataProvider,
            "campos"=>['tracking_pedido', 'carpeta_pedido', 'estado_pedido'],
            "titulo"=> "MUESTRA DE TODOS LOS PEDIDOS AGRUPADOS POR FECHA DE ENCARGO",
            "enunciado"=> "EJEMPLO DEL CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM pedidos group by fecha_encargo",
            
        ]);
 } 
//  
}

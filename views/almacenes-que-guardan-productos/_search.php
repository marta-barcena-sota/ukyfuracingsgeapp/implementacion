<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGuardanProductosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="almacenes-que-guardan-productos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_almacenamiento') ?>

    <?= $form->field($model, 'codigo_almacen') ?>

    <?= $form->field($model, 'codigo_producto') ?>

    <?= $form->field($model, 'fecha_ingreso_producto') ?>

    <?= $form->field($model, 'cantidad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

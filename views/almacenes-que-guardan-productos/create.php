<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGuardanProductos */

$this->title = 'Create Almacenes Que Guardan Productos';
$this->params['breadcrumbs'][] = ['label' => 'Almacenes Que Guardan Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="almacenes-que-guardan-productos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

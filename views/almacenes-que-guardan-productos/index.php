<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlmacenesQueGuardanProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Almacenes Que Guardan Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="almacenes-que-guardan-productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Almacenes Que Guardan Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_almacenamiento',
            'codigo_almacen',
            'codigo_producto',
            'fecha_ingreso_producto',
            'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

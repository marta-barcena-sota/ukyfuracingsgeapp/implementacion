<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGuardanProductos */

$this->title = 'Update Almacenes Que Guardan Productos: ' . $model->codigo_almacenamiento;
$this->params['breadcrumbs'][] = ['label' => 'Almacenes Que Guardan Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_almacenamiento, 'url' => ['view', 'id' => $model->codigo_almacenamiento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="almacenes-que-guardan-productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

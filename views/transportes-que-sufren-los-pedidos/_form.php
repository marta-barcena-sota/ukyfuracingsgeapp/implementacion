<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportes-que-sufren-los-pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'codigo_transporte')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

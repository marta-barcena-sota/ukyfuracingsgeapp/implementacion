<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportes-que-sufren-los-pedidos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_viaje') ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?= $form->field($model, 'codigo_transporte') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

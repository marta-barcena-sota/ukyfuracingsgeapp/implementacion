<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidos */

$this->title = 'Update Transportes Que Sufren Los Pedidos: ' . $model->codigo_viaje;
$this->params['breadcrumbs'][] = ['label' => 'Transportes Que Sufren Los Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_viaje, 'url' => ['view', 'id' => $model->codigo_viaje]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transportes-que-sufren-los-pedidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidos */

$this->title = 'Create Transportes Que Sufren Los Pedidos';
$this->params['breadcrumbs'][] = ['label' => 'Transportes Que Sufren Los Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportes-que-sufren-los-pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

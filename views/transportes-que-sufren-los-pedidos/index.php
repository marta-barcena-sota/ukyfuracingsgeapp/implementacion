<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransportesQueSufrenLosPedidosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transportes Que Sufren Los Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportes-que-sufren-los-pedidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transportes Que Sufren Los Pedidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_viaje',
            'codigo_pedido',
            'codigo_transporte',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cif_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eori_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_de_empresa_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sociedad_empresa_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pais_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_proveedor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'cif_proveedor') ?>

    <?= $form->field($model, 'eori_proveedor') ?>

    <?= $form->field($model, 'nombre_proveedor') ?>

    <?= $form->field($model, 'tipo_de_empresa_proveedor') ?>

    <?php // echo $form->field($model, 'sociedad_empresa_proveedor') ?>

    <?php // echo $form->field($model, 'pais_proveedor') ?>

    <?php // echo $form->field($model, 'tipo_proveedor') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueSufrenCambios */

$this->title = 'Update Productos Que Sufren Cambios: ' . $model->codigo_traspaso;
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Sufren Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_traspaso, 'url' => ['view', 'id' => $model->codigo_traspaso]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-que-sufren-cambios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

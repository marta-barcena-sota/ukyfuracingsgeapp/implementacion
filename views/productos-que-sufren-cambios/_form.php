<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueSufrenCambios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-sufren-cambios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <?= $form->field($model, 'codigo_cambio')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

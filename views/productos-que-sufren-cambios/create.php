<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueSufrenCambios */

$this->title = 'Create Productos Que Sufren Cambios';
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Sufren Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-sufren-cambios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosQueSufrenCambiosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Que Sufren Cambios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-sufren-cambios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Que Sufren Cambios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_traspaso',
            'codigo_producto',
            'codigo_cambio',
            'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

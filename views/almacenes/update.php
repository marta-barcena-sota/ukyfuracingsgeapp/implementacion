<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Almacenes */

$this->title = 'Update Almacenes: ' . $model->codigo_almacen;
$this->params['breadcrumbs'][] = ['label' => 'Almacenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_almacen, 'url' => ['view', 'id' => $model->codigo_almacen]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="almacenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

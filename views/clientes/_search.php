<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_cliente') ?>

    <?= $form->field($model, 'pais_cliente') ?>

    <?= $form->field($model, 'cif_cliente') ?>

    <?= $form->field($model, 'nombre_cliente') ?>

    <?= $form->field($model, 'tipo_empresa_cliente') ?>

    <?php // echo $form->field($model, 'sociedad_empresa_cliente') ?>

    <?php // echo $form->field($model, 'nombre_contacto_cliente') ?>

    <?php // echo $form->field($model, 'primer_apellido_contacto_cliente') ?>

    <?php // echo $form->field($model, 'segundo_apellido_contacto_cliente') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

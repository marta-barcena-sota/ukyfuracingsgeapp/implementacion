<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pais_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cif_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_empresa_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sociedad_empresa_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_contacto_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primer_apellido_contacto_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segundo_apellido_contacto_cliente')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

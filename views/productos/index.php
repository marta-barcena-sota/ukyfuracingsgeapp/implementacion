<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_producto',
            'cantidad_en_stock',
            'forma_de_almacenamiento',
            'concepto_producto',
            'primera_categoria_producto',
            //'segunda_categoria_producto',
            //'tercera_categoria_producto',
            //'referencia_interna_producto',
            //'referencia_articulo_producto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

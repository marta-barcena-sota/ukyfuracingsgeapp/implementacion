<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_producto') ?>

    <?= $form->field($model, 'cantidad_en_stock') ?>

    <?= $form->field($model, 'forma_de_almacenamiento') ?>

    <?= $form->field($model, 'concepto_producto') ?>

    <?= $form->field($model, 'primera_categoria_producto') ?>

    <?php // echo $form->field($model, 'segunda_categoria_producto') ?>

    <?php // echo $form->field($model, 'tercera_categoria_producto') ?>

    <?php // echo $form->field($model, 'referencia_interna_producto') ?>

    <?php // echo $form->field($model, 'referencia_articulo_producto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

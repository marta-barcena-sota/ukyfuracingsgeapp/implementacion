<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cantidad_en_stock')->textInput() ?>

    <?= $form->field($model, 'forma_de_almacenamiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'concepto_producto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primera_categoria_producto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segunda_categoria_producto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tercera_categoria_producto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia_interna_producto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia_articulo_producto')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsEmpleados */

$this->title = 'Update Tlfs Empleados: ' . $model->codigo_tlf;
$this->params['breadcrumbs'][] = ['label' => 'Tlfs Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tlf, 'url' => ['view', 'id' => $model->codigo_tlf]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tlfs-empleados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

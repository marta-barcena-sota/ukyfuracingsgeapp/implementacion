<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsEmpleados */

$this->title = 'Create Tlfs Empleados';
$this->params['breadcrumbs'][] = ['label' => 'Tlfs Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tlfs-empleados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenOrdenes */

$this->title = 'Create Productos Que Componen Ordenes';
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Componen Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-componen-ordenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-componen-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <?= $form->field($model, 'codigo_orden')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

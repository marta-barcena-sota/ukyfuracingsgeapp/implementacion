<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenOrdenes */

$this->title = 'Update Productos Que Componen Ordenes: ' . $model->codigo_componente;
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Componen Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_componente, 'url' => ['view', 'id' => $model->codigo_componente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-que-componen-ordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

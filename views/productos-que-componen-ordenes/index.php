<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosQueComponenOrdenesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Que Componen Ordenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-componen-ordenes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Que Componen Ordenes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_componente',
            'codigo_producto',
            'codigo_orden',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

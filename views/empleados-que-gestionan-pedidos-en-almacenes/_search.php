<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-gestionan-pedidos-en-almacenes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_gestion') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?= $form->field($model, 'codigo_almacen') ?>

    <?= $form->field($model, 'inventario') ?>

    <?php // echo $form->field($model, 'contabilidad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */

$this->title = 'Create Empleados Que Gestionan Pedidos En Almacenes';
$this->params['breadcrumbs'][] = ['label' => 'Empleados Que Gestionan Pedidos En Almacenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-que-gestionan-pedidos-en-almacenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

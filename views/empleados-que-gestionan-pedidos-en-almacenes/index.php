<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpleadosQueGestionanPedidosEnAlmacenesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados Que Gestionan Pedidos En Almacenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-que-gestionan-pedidos-en-almacenes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Empleados Que Gestionan Pedidos En Almacenes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_gestion',
            'codigo_empleado',
            'codigo_pedido',
            'codigo_almacen',
            'inventario',
            //'contabilidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

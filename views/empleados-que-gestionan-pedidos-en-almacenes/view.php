<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */

$this->title = $model->codigo_gestion;
$this->params['breadcrumbs'][] = ['label' => 'Empleados Que Gestionan Pedidos En Almacenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="empleados-que-gestionan-pedidos-en-almacenes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_gestion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_gestion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_gestion',
            'codigo_empleado',
            'codigo_pedido',
            'codigo_almacen',
            'inventario',
            'contabilidad',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */

$this->title = 'Update Empleados Que Gestionan Pedidos En Almacenes: ' . $model->codigo_gestion;
$this->params['breadcrumbs'][] = ['label' => 'Empleados Que Gestionan Pedidos En Almacenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_gestion, 'url' => ['view', 'id' => $model->codigo_gestion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empleados-que-gestionan-pedidos-en-almacenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-gestionan-pedidos-en-almacenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'codigo_almacen')->textInput() ?>

    <?= $form->field($model, 'inventario')->textInput() ?>

    <?= $form->field($model, 'contabilidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

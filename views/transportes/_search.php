<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_transporte') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'fecha_llegada') ?>

    <?= $form->field($model, 'fecha_estimada_llegada') ?>

    <?= $form->field($model, 'base_imponible_envio') ?>

    <?php // echo $form->field($model, 'fecha_envio_transporte') ?>

    <?php // echo $form->field($model, 'tipo_transporte') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

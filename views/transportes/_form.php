<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transportes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'fecha_llegada')->textInput() ?>

    <?= $form->field($model, 'fecha_estimada_llegada')->textInput() ?>

    <?= $form->field($model, 'base_imponible_envio')->textInput() ?>

    <?= $form->field($model, 'fecha_envio_transporte')->textInput() ?>

    <?= $form->field($model, 'tipo_transporte')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

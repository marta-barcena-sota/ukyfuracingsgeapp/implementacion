<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'LISTA DE POSIBLES CASOS DE USO';

?>

 <div class="jumbotron">
        <h1> CASOS DE USO </h1>
    </div>
    
    <div class="body-content">
        
       
                        <h3>POSIBLES CASOS DE USO PARA LOS PEDIDOS</h3>
                        <ul >
                            <li> Muestra de todos los pedidos registrados</li>
                            <ul style="list-style-type: none">
                                <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta1a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta1b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                            </ul>
                             <li>Muestra únicamente de los envíos registrados</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta2a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                 <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta2b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                             </ul>
                             
                            <li>Muestra únicamente de las recepciones registrados</li>
                            <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta3a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta3b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                             </ul>
                            
                             <li>Muestra de los pedidos facilitados por proveedores</li>
                            <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta4a'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta4b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                             </ul>
                         <li>Muestra únicamente de los pedidos encargados por clientes</li>
                            <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta5a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta5b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                             </ul>
                         <li>Muestra de los pedidos agrupados por estados</li>
                         <ul style="list-style-type: lower-roman">
                                 <li>PREVIO A PROCESO</li>
                                  <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta6a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta6b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                  <li>NUEVA ORDEN</li>
                                  <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta7a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta7b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                  <li>ACEPTADA</li> 
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta8a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta8b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                  <li> EN PROCESO </li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta9a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta9b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                   </ul>
                                  <li>PENDIENTE DE PAGO</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta10a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta10b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                   </ul>
                                  <li>ENVIADO</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta11a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta11b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                  
                                  <li>RECIBIDO</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta12a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta12b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                  
                                   <li>PDTE FACTURA</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta13a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta13b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                   
                                    <li>COMPLETADO</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta14a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta14b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                    
                                     <li>RECIBIDO DE FORMA PARCIAL</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta15a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta15b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                                     
                                                                          <li>INCIDENCIA DE ENVÍO</li>
                                   <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta16a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta16b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                             </ul>
                         <li>Agrupación por urgencia</li>
                         <ul style="list-style-type: lower-roman">
                             <li> PRIORIDAD ALTA </li>
                              <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta17a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta17b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                             <li> PRIORIDAD MEDIA </li>
                              <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta18a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta18b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                             <li> PRIORIDAD BAJA </li>
                              <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta19a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta19b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                 
                                    </ul>
                             
                         </ul>
                         <li>Agrupación por formas de pago</li>
                         <ul style="list-style-type: lower-roman">
                             <li>TRANSFERENCIA</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta20a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta20b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul>
                             <li>DOMICILIADO</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta21a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta21b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                              <li>PAYPAL</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta22a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta22b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                               <li>TARJETA ****9290</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta23a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta23b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                                <li>TARJETA ****4504</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta24a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta24b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                                 <li>EFECTIVO</li>
                             <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta25a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta25b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                         </ul>
                         <li>Pedidos sin clientes ni proveedores</li>
                          <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta26a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta26b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                         <li>Agrupacion por fechas de encargo</li>
                          <ul style="list-style-type: none">
                                 <li><?=Html::a ('CONSULTA CON DAO', ['site/consulta27a'] /*,['class'=> 'my-btn btn-primary']*/)?></li>
                                  <li><?= Html::a ('CONSULTA CON ORM', ['pedidos/consulta27b'] ,/*['class'=> 'my-btn btn-primary']*/)?></li>
                                    </ul> 
                        </ul>
             
        
    </div>
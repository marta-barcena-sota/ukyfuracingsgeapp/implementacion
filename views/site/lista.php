<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'LISTA DE CRUDS';

?>

<div > 
   
<div class="jumbotron">
        <h1>LISTADO DE CRUDS DE LA APLICACION</h1>
    </div>
    
    <div class="body-content">
        
        <li><?=Html::a ('PROVEEDORES', ['proveedores/index'])?></li>
        <li><?=Html::a ('TELÉFONOS DE LOS PROVEEDORES', ['telefonos-proveedores/index'])?></li>
        <li><?=Html::a ('EMAILS DE LOS PROVEEDORES', ['emails-proveedores/index'])?></li>
        <li><?=Html::a ('EMPLEADOS', ['empleados/index'])?></li>
        <li><?=Html::a ('TELÉFONOS DE LOS EMPLEADOS', ['tlfs-empleados/index'])?></li>
        <li><?=Html::a ('EMAILS DE LOS EMPLEADOS', ['emails-empleados/index'])?></li>
        <li><?=Html::a ('ALMACENES', ['almacenes/index'])?></li>
        <li><?=Html::a ('PRODUCTOS', ['productos/index'])?></li>
        <li><?=Html::a ('CLIENTES', ['clientes/index'])?></li>    
        <li><?=Html::a ('DIRECCIONES DE ENVÍO CLIENTES', ['direcciones-envio-clientes/index'])?></li>
        <li><?=Html::a ('DIRECCIONES DE FACTURACIÓN CLIENTES', ['direcciones-facturacion-clientes/index'])?></li>
        <li><?=Html::a ('TELÉFONOS DE LOS CLIENTES', ['tlfs-clientes/index'])?></li>  
        <li><?=Html::a ('EMAILS DE LOS CLIENTES', ['emails-clientes/index'])?></li> 
        <li><?=Html::a ('CAMBIOS', ['cambios/index'])?></li> 
        <li><?=Html::a ('INTERMEDIARIOS', ['intermediarios/index'])?></li> 
        <li><?=Html::a ('EMAILS DE LOS INTERMEDIARIOS', ['emails-intermediarios/index'])?></li> 
        <li><?=Html::a ('TELÉFONOS DE LOS INTERMEDIARIOS ', ['tlfs-intermediarios/index'])?></li>  
        <li><?=Html::a ('ENLACES DE LOS INTERMEDIARIOS', ['enlaces-intermediarios/index'])?></li> 
        <li><?=Html::a ('MATERIALES', ['materiales/index'])?></li> 
        <li><?=Html::a ('POSTPROCESADOS', ['postprocesados/index'])?></li>  
        <li><?=Html::a ('MAQUINAS', ['maquinas/index'])?></li>  
        <li><?=Html::a ('PROYECTOS', ['proyectos/index'])?></li> 
        <li><?=Html::a ('ÓRDENES DE FABRICACIÓN', ['ordenes-de-fabricacion/index'])?></li> 
        <li><?=Html::a ('MEJORAS', ['mejoras/index'])?></li>  
        <li><?=Html::a ('MANTENIMIENTOS', ['mantenimientos/index'])?></li> 
        <li><?=Html::a ('EMPLEADOS QUE TRABAJAN EN PROYECTOS', ['empleados-que-trabajan-en-proyectos/index'])?></li> 
        <li><?=Html::a ('COMPRAS', ['compras/index'])?></li> 
        <li><?=Html::a ('PEDIDOS', ['pedidos/index'])?></li>  
        <li><?=Html::a ('TRANSPORTES', ['transportes/index'])?></li> 
        <li><?=Html::a ('DOCUMENTACIONES', ['documentaciones/index'])?></li>  
        <li><?=Html::a ('SEGUIMIENTOS', ['seguimientos/index'])?></li>  
        <li><?=Html::a ('ENLACES DE LOS SEGUIMIENTOS', ['enlaces-seguimientos/index'])?></li>  
        <li><?=Html::a ('PRODUCTOS QUE COMPONEN PEDIDOS', ['productos-que-componen-pedidos/index'])?></li> 
        <li><?=Html::a ('COMENTARIOS', ['comentarios/index'])?></li>  
        <li><?=Html::a ('TRANSPORTES QUE SUFREN LOS PEDIDOS', ['transportes-que-sufren-los-pedidos/index'])?></li>  
        <li><?=Html::a ('ALMACENES QUE GESTIONAN CAMBIOS', ['almacenes-que-gestionan-cambios/index'])?></li> 
        <li><?=Html::a ('ALMACENES QUE GUARDAN PRODUCTOS ', ['almacenes-que-guardan-productos/index'])?></li> 
        <li><?=Html::a ('PRODUCTOS QUE COMPONEN ÓRDENES', ['productos-que-componen-ordenes/index'])?></li> 
        <li><?=Html::a ('PROVEEDORES QUE COLABORAN EN ÓRDENES', ['proveedores-que-colaboran-en-ordenes/index'])?></li>  
        <li><?=Html::a ('MAQUINAS QUE PARTICIPAN EN ÓRDENES', ['maquinas-participan-ordenes/index'])?></li> 
        <li><?=Html::a ('MEJORAS REALIZADAS A LAS MÁQUINAS', ['mejoras-de-maquinas/index'])?></li> 
        <li><?=Html::a ('PROVEEDORES QUE PARTICIPAN EN PROYECTOS', ['proveedores-que-participan-en-proyectos/index'])?></li>
        <li><?=Html::a ('INTERMEDIARIOS QUE PARTICIPAN EN PROYECTOS', ['intermediarios-que-participan-en-proyectos/index'])?></li>
        <li><?=Html::a ('PRODUCTOS QUE COMPONEN PROYECTOS', ['productos-componen-proyectos/index'])?></li>
        <li><?=Html::a ('PRODUCTOS QUE SUFREN CAMBIOS', ['productos-que-sufren-cambios/index'])?></li>
        <li><?=Html::a ('PRODUCTOS QUE COMPONEN PRODUCTOS', ['productos-que-componen-productos/index'])?></li>
        <li><?=Html::a ('VENTAS', ['ventas/index'])?></li> 
        <li><?=Html::a ('PROVEEDORES QUE SUMINISTRAN PRODUCTOS', ['proveedores-que-suministran-productos/index'])?></li>
        <li><?=Html::a ('PROVEEDORES QUE SUMINISTRAN MATERIALES', ['proveedores-que-suministran-materiales/index'])?></li>
        <li><?=Html::a ('PROVEEDORES QUE SUMINISTRAN POSTPROCESADOS', ['proveedores-que-suministran-postprocesados/index'])?></li>
        <li><?=Html::a ('EMPLEADOS QUE GESTIONAN PEDIDOS EN ALMACENES', ['empleados-que-gestionan-pedidos-en-almacenes/index'])?></li>
        <li><?=Html::a ('PROVEEDORES QUE SUMINISTRAN MATERIALES A ÓRDENES', ['proveedores-materiales-ordenes/index'])?></li>
        <li><?=Html::a ('PROVEEDORES QUE SUMINISTRAN POSTPROCESADOS A ÓRDENES', ['proveedores-que-suministran-postprocesados-a-ordenes/index'])?></li>
    </div>
    
</div>

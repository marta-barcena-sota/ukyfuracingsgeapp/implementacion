<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntermediariosQueParticipanEnProyectosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Intermediarios Que Participan En Proyectos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intermediarios-que-participan-en-proyectos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Intermediarios Que Participan En Proyectos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_participacion',
            'codigo_intermediario',
            'codigo_proyecto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostprocesadosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postprocesados-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_postprocesado') ?>

    <?= $form->field($model, 'fecha_inicio_postprocesado') ?>

    <?= $form->field($model, 'fecha_fin_postprocesado') ?>

    <?= $form->field($model, 'descripcion_postprocesado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

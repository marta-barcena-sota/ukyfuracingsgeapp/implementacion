<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesIntermediarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enlaces-intermediarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_intermediario')->textInput() ?>

    <?= $form->field($model, 'enlace_intermediario')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesIntermediarios */

$this->title = 'Update Enlaces Intermediarios: ' . $model->codigo_enlace;
$this->params['breadcrumbs'][] = ['label' => 'Enlaces Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_enlace, 'url' => ['view', 'id' => $model->codigo_enlace]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="enlaces-intermediarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

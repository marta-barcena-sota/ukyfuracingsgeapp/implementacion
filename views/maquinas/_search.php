<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maquinas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_maquina') ?>

    <?= $form->field($model, 'nombre_maquina') ?>

    <?= $form->field($model, 'numero_de_serie_maquina') ?>

    <?= $form->field($model, 'software_maquina') ?>

    <?= $form->field($model, 'manual_mantenimiento_maquina') ?>

    <?php // echo $form->field($model, 'modelo_maquina') ?>

    <?php // echo $form->field($model, 'manual_procedimiento_maquina') ?>

    <?php // echo $form->field($model, 'carpeta_maquina') ?>

    <?php // echo $form->field($model, 'tipo_maquina') ?>

    <?php // echo $form->field($model, 'descripcion_maquina') ?>

    <?php // echo $form->field($model, 'manual_maquina') ?>

    <?php // echo $form->field($model, 'firmware_maquina') ?>

    <?php // echo $form->field($model, 'fabricante_maquina') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

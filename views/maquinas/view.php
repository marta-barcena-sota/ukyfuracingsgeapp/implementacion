<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Maquinas */

$this->title = $model->codigo_maquina;
$this->params['breadcrumbs'][] = ['label' => 'Maquinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="maquinas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_maquina], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_maquina], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_maquina',
            'nombre_maquina',
            'numero_de_serie_maquina',
            'software_maquina',
            'manual_mantenimiento_maquina',
            'modelo_maquina',
            'manual_procedimiento_maquina',
            'carpeta_maquina',
            'tipo_maquina',
            'descripcion_maquina',
            'manual_maquina',
            'firmware_maquina',
            'fabricante_maquina',
        ],
    ]) ?>

</div>

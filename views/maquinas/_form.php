<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Maquinas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maquinas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_de_serie_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'software_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manual_mantenimiento_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelo_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manual_procedimiento_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manual_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firmware_maquina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fabricante_maquina')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SeguimientosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguimientos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_seguimiento') ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?= $form->field($model, 'codigo_orden_de_fabricacion') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'hora_seguimiento') ?>

    <?php // echo $form->field($model, 'fecha_de_realizacion_seguimiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

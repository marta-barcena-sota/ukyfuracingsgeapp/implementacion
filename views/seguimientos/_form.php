<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Seguimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'codigo_orden_de_fabricacion')->textInput() ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'hora_seguimiento')->textInput() ?>

    <?= $form->field($model, 'fecha_de_realizacion_seguimiento')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

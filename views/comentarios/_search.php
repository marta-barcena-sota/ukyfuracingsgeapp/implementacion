<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ComentariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comentarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_comentario') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'codigo_seguimiento') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?php // echo $form->field($model, 'codigo_producto') ?>

    <?php // echo $form->field($model, 'codigo_cliente') ?>

    <?php // echo $form->field($model, 'codigo_cambio') ?>

    <?php // echo $form->field($model, 'codigo_intermediario') ?>

    <?php // echo $form->field($model, 'concepto_comentario') ?>

    <?php // echo $form->field($model, 'fecha_comentario') ?>

    <?php // echo $form->field($model, 'hora_comentario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

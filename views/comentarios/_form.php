<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comentarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comentarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'codigo_seguimiento')->textInput() ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <?= $form->field($model, 'codigo_cambio')->textInput() ?>

    <?= $form->field($model, 'codigo_intermediario')->textInput() ?>

    <?= $form->field($model, 'concepto_comentario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_comentario')->textInput() ?>

    <?= $form->field($model, 'hora_comentario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

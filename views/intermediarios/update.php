<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intermediarios */

$this->title = 'Update Intermediarios: ' . $model->codigo_intermediario;
$this->params['breadcrumbs'][] = ['label' => 'Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_intermediario, 'url' => ['view', 'id' => $model->codigo_intermediario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="intermediarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Intermediarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intermediarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'eori_intermediario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cif_intermediario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_intermediario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_empresa_intermediario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sociedad_empresa_intermediario')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

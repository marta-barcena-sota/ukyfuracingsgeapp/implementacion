<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intermediarios */

$this->title = 'Create Intermediarios';
$this->params['breadcrumbs'][] = ['label' => 'Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intermediarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

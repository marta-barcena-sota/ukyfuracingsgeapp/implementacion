<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IntermediariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intermediarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_intermediario') ?>

    <?= $form->field($model, 'eori_intermediario') ?>

    <?= $form->field($model, 'cif_intermediario') ?>

    <?= $form->field($model, 'nombre_intermediario') ?>

    <?= $form->field($model, 'tipo_empresa_intermediario') ?>

    <?php // echo $form->field($model, 'sociedad_empresa_intermediario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

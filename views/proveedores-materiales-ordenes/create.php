<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresMaterialesOrdenes */

$this->title = 'Create Proveedores Materiales Ordenes';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Materiales Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-materiales-ordenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

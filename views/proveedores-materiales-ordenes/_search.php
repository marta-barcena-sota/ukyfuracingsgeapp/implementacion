<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresMaterialesOrdenesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-materiales-ordenes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_suministro') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'codigo_material') ?>

    <?= $form->field($model, 'codigo_orden') ?>

    <?= $form->field($model, 'cantidad') ?>

    <?php // echo $form->field($model, 'medida') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

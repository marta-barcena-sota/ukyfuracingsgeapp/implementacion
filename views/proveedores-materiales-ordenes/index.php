<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresMaterialesOrdenesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores Materiales Ordenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-materiales-ordenes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores Materiales Ordenes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_suministro',
            'codigo_proveedor',
            'codigo_material',
            'codigo_orden',
            'cantidad',
            //'medida',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

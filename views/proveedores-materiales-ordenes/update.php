<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresMaterialesOrdenes */

$this->title = 'Update Proveedores Materiales Ordenes: ' . $model->codigo_suministro;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Materiales Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_suministro, 'url' => ['view', 'id' => $model->codigo_suministro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proveedores-materiales-ordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresMaterialesOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-materiales-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'codigo_material')->textInput() ?>

    <?= $form->field($model, 'codigo_orden')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'medida')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

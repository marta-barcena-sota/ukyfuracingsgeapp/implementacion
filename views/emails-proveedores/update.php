<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsProveedores */

$this->title = 'Update Emails Proveedores: ' . $model->codigo_email_proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Emails Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_email_proveedor, 'url' => ['view', 'id' => $model->codigo_email_proveedor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emails-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

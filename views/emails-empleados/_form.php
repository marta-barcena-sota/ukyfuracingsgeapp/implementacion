<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsEmpleados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-empleados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'email_empleado')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

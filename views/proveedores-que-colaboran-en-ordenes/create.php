<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueColaboranEnOrdenes */

$this->title = 'Create Proveedores Que Colaboran En Ordenes';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Colaboran En Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-colaboran-en-ordenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueColaboranEnOrdenesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-colaboran-en-ordenes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_colaboracion') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'codigo_orden_fabricacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

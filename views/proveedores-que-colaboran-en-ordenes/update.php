<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueColaboranEnOrdenes */

$this->title = 'Update Proveedores Que Colaboran En Ordenes: ' . $model->codigo_colaboracion;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Colaboran En Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_colaboracion, 'url' => ['view', 'id' => $model->codigo_colaboracion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proveedores-que-colaboran-en-ordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

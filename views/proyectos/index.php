<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProyectosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proyectos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyectos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proyectos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_proyecto',
            'codigo_cliente',
            'referencia_interna_proyecto',
            'nombre_proyecto',
            'carpeta_proyecto',
            //'referencia_cad_proyecto',
            //'gantt_proyecto',
            //'fecha_fin_proyecto',
            //'fecha_inicio_proyecto',
            //'tipo_proyecto',
            //'estado_proyecto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

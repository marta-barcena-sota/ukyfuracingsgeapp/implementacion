<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <?= $form->field($model, 'referencia_interna_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia_cad_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gantt_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_fin_proyecto')->textInput() ?>

    <?= $form->field($model, 'fecha_inicio_proyecto')->textInput() ?>

    <?= $form->field($model, 'tipo_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_proyecto')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProyectosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_proyecto') ?>

    <?= $form->field($model, 'codigo_cliente') ?>

    <?= $form->field($model, 'referencia_interna_proyecto') ?>

    <?= $form->field($model, 'nombre_proyecto') ?>

    <?= $form->field($model, 'carpeta_proyecto') ?>

    <?php // echo $form->field($model, 'referencia_cad_proyecto') ?>

    <?php // echo $form->field($model, 'gantt_proyecto') ?>

    <?php // echo $form->field($model, 'fecha_fin_proyecto') ?>

    <?php // echo $form->field($model, 'fecha_inicio_proyecto') ?>

    <?php // echo $form->field($model, 'tipo_proyecto') ?>

    <?php // echo $form->field($model, 'estado_proyecto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

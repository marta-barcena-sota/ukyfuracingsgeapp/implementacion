<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proyectos */

$this->title = $model->codigo_proyecto;
$this->params['breadcrumbs'][] = ['label' => 'Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="proyectos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_proyecto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_proyecto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_proyecto',
            'codigo_cliente',
            'referencia_interna_proyecto',
            'nombre_proyecto',
            'carpeta_proyecto',
            'referencia_cad_proyecto',
            'gantt_proyecto',
            'fecha_fin_proyecto',
            'fecha_inicio_proyecto',
            'tipo_proyecto',
            'estado_proyecto',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */

$this->title = 'Create Proveedores Que Suministran Materiales';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Suministran Materiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-materiales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

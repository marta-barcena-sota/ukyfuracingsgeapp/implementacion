<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresQueSuministranMaterialesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores Que Suministran Materiales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-materiales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores Que Suministran Materiales', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_suministro',
            'codigo_proveedor',
            'codigo_material',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-suministran-materiales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'codigo_material')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MaterialesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materiales-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_material') ?>

    <?= $form->field($model, 'familia_material') ?>

    <?= $form->field($model, 'clase_material') ?>

    <?= $form->field($model, 'tipo_material') ?>

    <?= $form->field($model, 'carpeta_material') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

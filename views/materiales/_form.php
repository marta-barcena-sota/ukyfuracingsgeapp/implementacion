<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Materiales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materiales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'familia_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clase_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_material')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesFacturacionClientes */

$this->title = 'Update Direcciones Facturacion Clientes: ' . $model->codigo_direccion_facturacion;
$this->params['breadcrumbs'][] = ['label' => 'Direcciones Facturacion Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_direccion_facturacion, 'url' => ['view', 'id' => $model->codigo_direccion_facturacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="direcciones-facturacion-clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

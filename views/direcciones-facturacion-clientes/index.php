<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DireccionesFacturacionClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Direcciones Facturacion Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direcciones-facturacion-clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Direcciones Facturacion Clientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_direccion_facturacion',
            'codigo_cliente',
            'direccion_facturacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

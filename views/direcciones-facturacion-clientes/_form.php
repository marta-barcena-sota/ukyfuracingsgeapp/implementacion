<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesFacturacionClientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="direcciones-facturacion-clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <?= $form->field($model, 'direccion_facturacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

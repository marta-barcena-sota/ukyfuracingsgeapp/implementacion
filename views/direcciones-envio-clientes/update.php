<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesEnvioClientes */

$this->title = 'Update Direcciones Envio Clientes: ' . $model->codigo_direccion_envio;
$this->params['breadcrumbs'][] = ['label' => 'Direcciones Envio Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_direccion_envio, 'url' => ['view', 'id' => $model->codigo_direccion_envio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="direcciones-envio-clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

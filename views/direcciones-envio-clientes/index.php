<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DireccionesEnvioClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Direcciones Envio Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direcciones-envio-clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Direcciones Envio Clientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_direccion_envio',
            'codigo_cliente',
            'direccion_envio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

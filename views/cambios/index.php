<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CambiosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cambios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cambios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Cambios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_cambio',
            'motivo_cambio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

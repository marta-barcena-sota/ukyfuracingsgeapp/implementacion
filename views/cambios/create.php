<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cambios */

$this->title = 'Create Cambios';
$this->params['breadcrumbs'][] = ['label' => 'Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cambios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

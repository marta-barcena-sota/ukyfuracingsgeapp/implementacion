<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedidosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedidos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'codigo_cliente') ?>

    <?= $form->field($model, 'tracking_pedido') ?>

    <?= $form->field($model, 'carpeta_pedido') ?>

    <?php // echo $form->field($model, 'tipo_pedido') ?>

    <?php // echo $form->field($model, 'estado_pedido') ?>

    <?php // echo $form->field($model, 'urgencia_pedido') ?>

    <?php // echo $form->field($model, 'forma_de_pago') ?>

    <?php // echo $form->field($model, 'hay_pago_importacion') ?>

    <?php // echo $form->field($model, 'pago_importacion') ?>

    <?php // echo $form->field($model, 'fecha_suministro') ?>

    <?php // echo $form->field($model, 'fecha_encargo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

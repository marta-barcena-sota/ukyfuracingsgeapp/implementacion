<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <?= $form->field($model, 'tracking_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'urgencia_pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'forma_de_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hay_pago_importacion')->textInput() ?>

    <?= $form->field($model, 'pago_importacion')->textInput() ?>

    <?= $form->field($model, 'fecha_suministro')->textInput() ?>

    <?= $form->field($model, 'fecha_encargo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

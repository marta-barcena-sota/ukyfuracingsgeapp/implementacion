<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'MUESTRA DE TODOS LOS PEDIDOS';

?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
    <p class="lead"> <?= $enunciado ?> </p>
    <div class="well">
        <?= $sql ?>
        
    </div>
</div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
    'columns'=>$campos
        ]);?>

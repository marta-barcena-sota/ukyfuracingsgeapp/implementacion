<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesSeguimientosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enlaces-seguimientos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_enlace_seguimiento') ?>

    <?= $form->field($model, 'codigo_seguimiento') ?>

    <?= $form->field($model, 'enlace_seguimiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

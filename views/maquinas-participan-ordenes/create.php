<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasParticipanOrdenes */

$this->title = 'Create Maquinas Participan Ordenes';
$this->params['breadcrumbs'][] = ['label' => 'Maquinas Participan Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-participan-ordenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasParticipanOrdenes */

$this->title = 'Update Maquinas Participan Ordenes: ' . $model->codigo_operacion;
$this->params['breadcrumbs'][] = ['label' => 'Maquinas Participan Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_operacion, 'url' => ['view', 'id' => $model->codigo_operacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maquinas-participan-ordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasParticipanOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maquinas-participan-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_maquina')->textInput() ?>

    <?= $form->field($model, 'codigo_orden')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TelefonosProveedoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Telefonos Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonos-proveedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Telefonos Proveedores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_tlf_proveedor',
            'codigo_proveedor',
            'tlf_proveedor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

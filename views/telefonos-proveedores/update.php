<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonosProveedores */

$this->title = 'Update Telefonos Proveedores: ' . $model->codigo_tlf_proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tlf_proveedor, 'url' => ['view', 'id' => $model->codigo_tlf_proveedor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

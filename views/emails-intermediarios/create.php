<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsIntermediarios */

$this->title = 'Create Emails Intermediarios';
$this->params['breadcrumbs'][] = ['label' => 'Emails Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-intermediarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

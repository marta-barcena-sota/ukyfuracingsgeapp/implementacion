<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmailsIntermediariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emails Intermediarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-intermediarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Emails Intermediarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_email:email',
            'codigo_intermediario',
            'email_intermediario:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MejorasDeMaquinasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mejoras De Maquinas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mejoras-de-maquinas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mejoras De Maquinas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_aplicacion',
            'codigo_maquina',
            'codigo_mejora',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

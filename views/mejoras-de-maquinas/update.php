<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */

$this->title = 'Update Mejoras De Maquinas: ' . $model->codigo_aplicacion;
$this->params['breadcrumbs'][] = ['label' => 'Mejoras De Maquinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_aplicacion, 'url' => ['view', 'id' => $model->codigo_aplicacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mejoras-de-maquinas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

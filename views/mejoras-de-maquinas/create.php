<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */

$this->title = 'Create Mejoras De Maquinas';
$this->params['breadcrumbs'][] = ['label' => 'Mejoras De Maquinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mejoras-de-maquinas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

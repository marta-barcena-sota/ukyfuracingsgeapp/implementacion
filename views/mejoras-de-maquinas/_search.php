<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-de-maquinas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_aplicacion') ?>

    <?= $form->field($model, 'codigo_maquina') ?>

    <?= $form->field($model, 'codigo_mejora') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

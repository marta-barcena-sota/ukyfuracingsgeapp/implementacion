<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-de-maquinas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_maquina')->textInput() ?>

    <?= $form->field($model, 'codigo_mejora')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

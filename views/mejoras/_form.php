<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mejoras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'descripcion_mejora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_mejora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

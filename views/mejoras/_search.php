<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_mejora') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'descripcion_mejora') ?>

    <?= $form->field($model, 'carpeta_mejora') ?>

    <?= $form->field($model, 'fecha') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

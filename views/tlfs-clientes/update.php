<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsClientes */

$this->title = 'Update Tlfs Clientes: ' . $model->codigo_tlf;
$this->params['breadcrumbs'][] = ['label' => 'Tlfs Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tlf, 'url' => ['view', 'id' => $model->codigo_tlf]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tlfs-clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

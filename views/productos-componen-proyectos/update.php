<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosComponenProyectos */

$this->title = 'Update Productos Componen Proyectos: ' . $model->codigo_componente;
$this->params['breadcrumbs'][] = ['label' => 'Productos Componen Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_componente, 'url' => ['view', 'id' => $model->codigo_componente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-componen-proyectos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

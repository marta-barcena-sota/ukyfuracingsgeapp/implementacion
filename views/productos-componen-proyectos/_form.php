<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosComponenProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-componen-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proyecto')->textInput() ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

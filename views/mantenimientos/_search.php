<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MantenimientosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mantenimientos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_mantenimiento') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'codigo_maquina') ?>

    <?= $form->field($model, 'version_firmware') ?>

    <?= $form->field($model, 'fecha_de_realizacion_mantenimiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mantenimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mantenimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'codigo_maquina')->textInput() ?>

    <?= $form->field($model, 'version_firmware')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_de_realizacion_mantenimiento')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

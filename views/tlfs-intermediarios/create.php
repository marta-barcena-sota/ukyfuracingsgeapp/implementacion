<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsIntermediarios */

$this->title = 'Create Tlfs Intermediarios';
$this->params['breadcrumbs'][] = ['label' => 'Tlfs Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tlfs-intermediarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

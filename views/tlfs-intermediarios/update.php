<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsIntermediarios */

$this->title = 'Update Tlfs Intermediarios: ' . $model->codigo_tlf;
$this->params['breadcrumbs'][] = ['label' => 'Tlfs Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tlf, 'url' => ['view', 'id' => $model->codigo_tlf]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tlfs-intermediarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosQueComponenPedidosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Que Componen Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-componen-pedidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Que Componen Pedidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_componente',
            'codigo_producto',
            'codigo_pedido',
            'cantidad',
            'cantidad_pendiente',
            //'realizado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenPedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-componen-pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'cantidad_pendiente')->textInput() ?>

    <?= $form->field($model, 'realizado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

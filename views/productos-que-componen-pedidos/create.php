<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenPedidos */

$this->title = 'Create Productos Que Componen Pedidos';
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Componen Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-componen-pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

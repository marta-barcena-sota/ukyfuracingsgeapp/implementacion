<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacion */

$this->title = 'Update Ordenes De Fabricacion: ' . $model->codigo_orden_de_fabricacion;
$this->params['breadcrumbs'][] = ['label' => 'Ordenes De Fabricacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_orden_de_fabricacion, 'url' => ['view', 'id' => $model->codigo_orden_de_fabricacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ordenes-de-fabricacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

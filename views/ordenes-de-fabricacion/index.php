<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdenesDeFabricacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ordenes De Fabricacions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-de-fabricacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ordenes De Fabricacion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_orden_de_fabricacion',
            'codigo_empleado',
            'codigo_proyecto',
            'nombre_orden_de_fabricacion',
            'imagen',
            //'estado',
            //'fecha_inicio_orden_de_fabricacion',
            //'fecha_fin_orden_de_fabricacion',
            //'tipo_orden_de_fabricacion',
            //'fecha_estimada_fin_orden_fabricacion',
            //'urgencia_orden_fabricacion',
            //'estimacion_coste_material',
            //'coste_fabricacion',
            //'coste_transporte',
            //'coste_postprocesado',
            //'coste_maquina',
            //'coste_mano_de_obra',
            //'coste_material',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacion */

$this->title = 'Create Ordenes De Fabricacion';
$this->params['breadcrumbs'][] = ['label' => 'Ordenes De Fabricacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenes-de-fabricacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

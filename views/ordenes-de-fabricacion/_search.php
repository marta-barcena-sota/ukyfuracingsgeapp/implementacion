<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ordenes-de-fabricacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_orden_de_fabricacion') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'codigo_proyecto') ?>

    <?= $form->field($model, 'nombre_orden_de_fabricacion') ?>

    <?= $form->field($model, 'imagen') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'fecha_inicio_orden_de_fabricacion') ?>

    <?php // echo $form->field($model, 'fecha_fin_orden_de_fabricacion') ?>

    <?php // echo $form->field($model, 'tipo_orden_de_fabricacion') ?>

    <?php // echo $form->field($model, 'fecha_estimada_fin_orden_fabricacion') ?>

    <?php // echo $form->field($model, 'urgencia_orden_fabricacion') ?>

    <?php // echo $form->field($model, 'estimacion_coste_material') ?>

    <?php // echo $form->field($model, 'coste_fabricacion') ?>

    <?php // echo $form->field($model, 'coste_transporte') ?>

    <?php // echo $form->field($model, 'coste_postprocesado') ?>

    <?php // echo $form->field($model, 'coste_maquina') ?>

    <?php // echo $form->field($model, 'coste_mano_de_obra') ?>

    <?php // echo $form->field($model, 'coste_material') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

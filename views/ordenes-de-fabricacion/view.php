<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacion */

$this->title = $model->codigo_orden_de_fabricacion;
$this->params['breadcrumbs'][] = ['label' => 'Ordenes De Fabricacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ordenes-de-fabricacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_orden_de_fabricacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_orden_de_fabricacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_orden_de_fabricacion',
            'codigo_empleado',
            'codigo_proyecto',
            'nombre_orden_de_fabricacion',
            'imagen',
            'estado',
            'fecha_inicio_orden_de_fabricacion',
            'fecha_fin_orden_de_fabricacion',
            'tipo_orden_de_fabricacion',
            'fecha_estimada_fin_orden_fabricacion',
            'urgencia_orden_fabricacion',
            'estimacion_coste_material',
            'coste_fabricacion',
            'coste_transporte',
            'coste_postprocesado',
            'coste_maquina',
            'coste_mano_de_obra',
            'coste_material',
        ],
    ]) ?>

</div>

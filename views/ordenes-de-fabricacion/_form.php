<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ordenes-de-fabricacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'codigo_proyecto')->textInput() ?>

    <?= $form->field($model, 'nombre_orden_de_fabricacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_inicio_orden_de_fabricacion')->textInput() ?>

    <?= $form->field($model, 'fecha_fin_orden_de_fabricacion')->textInput() ?>

    <?= $form->field($model, 'tipo_orden_de_fabricacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_estimada_fin_orden_fabricacion')->textInput() ?>

    <?= $form->field($model, 'urgencia_orden_fabricacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estimacion_coste_material')->textInput() ?>

    <?= $form->field($model, 'coste_fabricacion')->textInput() ?>

    <?= $form->field($model, 'coste_transporte')->textInput() ?>

    <?= $form->field($model, 'coste_postprocesado')->textInput() ?>

    <?= $form->field($model, 'coste_maquina')->textInput() ?>

    <?= $form->field($model, 'coste_mano_de_obra')->textInput() ?>

    <?= $form->field($model, 'coste_material')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

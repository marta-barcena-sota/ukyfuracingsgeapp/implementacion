<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */

$this->title = $model->codigo_documentacion;
$this->params['breadcrumbs'][] = ['label' => 'Documentaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="documentaciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_documentacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_documentacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_documentacion',
            'codigo_pedido',
            'codigo_producto',
            'documento_de_transporte',
            'justificante_de_pago',
            'numero_de_factura',
            'fecha_factura',
            'archivo_factura',
            'albaran',
            'pedido',
            'documento_importacion',
        ],
    ]) ?>

</div>

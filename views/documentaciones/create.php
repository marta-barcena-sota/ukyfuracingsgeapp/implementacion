<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */

$this->title = 'Create Documentaciones';
$this->params['breadcrumbs'][] = ['label' => 'Documentaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

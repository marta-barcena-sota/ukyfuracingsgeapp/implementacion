<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documentaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_documentacion') ?>

    <?= $form->field($model, 'codigo_pedido') ?>

    <?= $form->field($model, 'codigo_producto') ?>

    <?= $form->field($model, 'documento_de_transporte') ?>

    <?= $form->field($model, 'justificante_de_pago') ?>

    <?php // echo $form->field($model, 'numero_de_factura') ?>

    <?php // echo $form->field($model, 'fecha_factura') ?>

    <?php // echo $form->field($model, 'archivo_factura') ?>

    <?php // echo $form->field($model, 'albaran') ?>

    <?php // echo $form->field($model, 'pedido') ?>

    <?php // echo $form->field($model, 'documento_importacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

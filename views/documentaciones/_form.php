<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documentaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_pedido')->textInput() ?>

    <?= $form->field($model, 'codigo_producto')->textInput() ?>

    <?= $form->field($model, 'documento_de_transporte')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'justificante_de_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_de_factura')->textInput() ?>

    <?= $form->field($model, 'fecha_factura')->textInput() ?>

    <?= $form->field($model, 'archivo_factura')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'albaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'documento_importacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documentaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Documentaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_documentacion',
            'codigo_pedido',
            'codigo_producto',
            'documento_de_transporte',
            'justificante_de_pago',
            //'numero_de_factura',
            //'fecha_factura',
            //'archivo_factura',
            //'albaran',
            //'pedido',
            //'documento_importacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

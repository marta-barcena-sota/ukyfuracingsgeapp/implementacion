<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */

$this->title = 'Update Documentaciones: ' . $model->codigo_documentacion;
$this->params['breadcrumbs'][] = ['label' => 'Documentaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_documentacion, 'url' => ['view', 'id' => $model->codigo_documentacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="documentaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresQueSuministranPostprocesadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores Que Suministran Postprocesados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-postprocesados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores Que Suministran Postprocesados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_suministro',
            'codigo_proveedor',
            'codigo_postprocesado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

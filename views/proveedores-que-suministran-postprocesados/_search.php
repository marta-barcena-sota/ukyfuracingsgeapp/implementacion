<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesadosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-suministran-postprocesados-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_suministro') ?>

    <?= $form->field($model, 'codigo_proveedor') ?>

    <?= $form->field($model, 'codigo_postprocesado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

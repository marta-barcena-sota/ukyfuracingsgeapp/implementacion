<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesados */

$this->title = 'Create Proveedores Que Suministran Postprocesados';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Suministran Postprocesados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-postprocesados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

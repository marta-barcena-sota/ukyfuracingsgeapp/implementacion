<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueTrabajanEnProyectos */

$this->title = 'Update Empleados Que Trabajan En Proyectos: ' . $model->codigo_trabajo;
$this->params['breadcrumbs'][] = ['label' => 'Empleados Que Trabajan En Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_trabajo, 'url' => ['view', 'id' => $model->codigo_trabajo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empleados-que-trabajan-en-proyectos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

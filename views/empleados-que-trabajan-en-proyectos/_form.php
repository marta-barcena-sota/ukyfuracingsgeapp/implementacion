<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueTrabajanEnProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-trabajan-en-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proyecto')->textInput() ?>

    <?= $form->field($model, 'codigo_empleado')->textInput() ?>

    <?= $form->field($model, 'responsable')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

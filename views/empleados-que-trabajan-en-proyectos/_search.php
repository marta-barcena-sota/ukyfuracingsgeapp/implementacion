<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueTrabajanEnProyectosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-trabajan-en-proyectos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_trabajo') ?>

    <?= $form->field($model, 'codigo_proyecto') ?>

    <?= $form->field($model, 'codigo_empleado') ?>

    <?= $form->field($model, 'responsable') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

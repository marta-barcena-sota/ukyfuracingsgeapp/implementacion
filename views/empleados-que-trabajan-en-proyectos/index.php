<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpleadosQueTrabajanEnProyectosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados Que Trabajan En Proyectos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-que-trabajan-en-proyectos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Empleados Que Trabajan En Proyectos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_trabajo',
            'codigo_proyecto',
            'codigo_empleado',
            'responsable',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

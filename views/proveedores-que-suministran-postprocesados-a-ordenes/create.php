<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesadosAOrdenes */

$this->title = 'Create Proveedores Que Suministran Postprocesados A Ordenes';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Suministran Postprocesados A Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-postprocesados-aordenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

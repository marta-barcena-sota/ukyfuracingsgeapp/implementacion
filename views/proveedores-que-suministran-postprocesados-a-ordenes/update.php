<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesadosAOrdenes */

$this->title = 'Update Proveedores Que Suministran Postprocesados A Ordenes: ' . $model->codigo_suministro;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Suministran Postprocesados A Ordenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_suministro, 'url' => ['view', 'id' => $model->codigo_suministro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proveedores-que-suministran-postprocesados-aordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

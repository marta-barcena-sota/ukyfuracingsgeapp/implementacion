<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGestionanCambios */

$this->title = 'Update Almacenes Que Gestionan Cambios: ' . $model->codigo_gestion;
$this->params['breadcrumbs'][] = ['label' => 'Almacenes Que Gestionan Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_gestion, 'url' => ['view', 'id' => $model->codigo_gestion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="almacenes-que-gestionan-cambios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

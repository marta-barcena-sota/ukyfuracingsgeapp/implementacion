<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlmacenesQueGestionanCambiosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Almacenes Que Gestionan Cambios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="almacenes-que-gestionan-cambios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Almacenes Que Gestionan Cambios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_gestion',
            'codigo_almacen',
            'codigo_cambio',
            'origen',
            'destino',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

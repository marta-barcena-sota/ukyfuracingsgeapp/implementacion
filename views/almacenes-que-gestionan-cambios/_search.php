<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGestionanCambiosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="almacenes-que-gestionan-cambios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_gestion') ?>

    <?= $form->field($model, 'codigo_almacen') ?>

    <?= $form->field($model, 'codigo_cambio') ?>

    <?= $form->field($model, 'origen') ?>

    <?= $form->field($model, 'destino') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsClientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <?= $form->field($model, 'email_cliente')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

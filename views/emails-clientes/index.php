<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmailsClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emails Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Emails Clientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_email:email',
            'codigo_cliente',
            'email_cliente:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsClientes */

$this->title = 'Update Emails Clientes: ' . $model->codigo_email;
$this->params['breadcrumbs'][] = ['label' => 'Emails Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_email, 'url' => ['view', 'id' => $model->codigo_email]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emails-clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

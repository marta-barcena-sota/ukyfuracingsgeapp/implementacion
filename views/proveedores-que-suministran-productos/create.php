<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranProductos */

$this->title = 'Create Proveedores Que Suministran Productos';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Suministran Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-productos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresQueSuministranProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores Que Suministran Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores Que Suministran Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_suministro',
            'codigo_proveedor',
            'codigo_producto',
            'base_imponible_unitaria',
            'iva_impuesto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

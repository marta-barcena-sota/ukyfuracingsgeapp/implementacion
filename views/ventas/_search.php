<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VentasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ventas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_venta') ?>

    <?= $form->field($model, 'cod_cliente') ?>

    <?= $form->field($model, 'cod_producto') ?>

    <?= $form->field($model, 'cantidad') ?>

    <?= $form->field($model, 'base_imponible_unitaria_venta') ?>

    <?php // echo $form->field($model, 'beneficio') ?>

    <?php // echo $form->field($model, 'fecha_de_salida') ?>

    <?php // echo $form->field($model, 'referencia_interna_venta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

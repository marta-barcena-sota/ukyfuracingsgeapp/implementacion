<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ventas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ventas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_cliente')->textInput() ?>

    <?= $form->field($model, 'cod_producto')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'base_imponible_unitaria_venta')->textInput() ?>

    <?= $form->field($model, 'beneficio')->textInput() ?>

    <?= $form->field($model, 'fecha_de_salida')->textInput() ?>

    <?= $form->field($model, 'referencia_interna_venta')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

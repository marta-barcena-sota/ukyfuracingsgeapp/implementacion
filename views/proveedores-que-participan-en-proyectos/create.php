<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueParticipanEnProyectos */

$this->title = 'Create Proveedores Que Participan En Proyectos';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Participan En Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-participan-en-proyectos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueParticipanEnProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-participan-en-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_proveedor')->textInput() ?>

    <?= $form->field($model, 'codigo_proyecto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

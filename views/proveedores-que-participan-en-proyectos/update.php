<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueParticipanEnProyectos */

$this->title = 'Update Proveedores Que Participan En Proyectos: ' . $model->codigo_participacion;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores Que Participan En Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_participacion, 'url' => ['view', 'id' => $model->codigo_participacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proveedores-que-participan-en-proyectos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

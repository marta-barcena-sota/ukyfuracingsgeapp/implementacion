<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenProductos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-componen-productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'semielaborado')->textInput() ?>

    <?= $form->field($model, 'acabado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosQueComponenProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Que Componen Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-componen-productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Que Componen Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_componente',
            'semielaborado',
            'acabado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Postprocesados;

/**
 * PostprocesadosSearch represents the model behind the search form of `app\models\Postprocesados`.
 */
class PostprocesadosSearch extends Postprocesados
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_postprocesado'], 'integer'],
            [['fecha_inicio_postprocesado', 'fecha_fin_postprocesado', 'descripcion_postprocesado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Postprocesados::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_postprocesado' => $this->codigo_postprocesado,
            'fecha_inicio_postprocesado' => $this->fecha_inicio_postprocesado,
            'fecha_fin_postprocesado' => $this->fecha_fin_postprocesado,
        ]);

        $query->andFilterWhere(['like', 'descripcion_postprocesado', $this->descripcion_postprocesado]);

        return $dataProvider;
    }
}

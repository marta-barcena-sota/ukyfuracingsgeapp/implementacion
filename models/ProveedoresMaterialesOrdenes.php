<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_materiales_ordenes".
 *
 * @property int $codigo_suministro
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_material
 * @property int|null $codigo_orden
 * @property int|null $cantidad
 * @property string|null $medida
 *
 * @property Proveedores $codigoProveedor
 * @property Materiales $codigoMaterial
 * @property OrdenesDeFabricacion $codigoOrden
 */
class ProveedoresMaterialesOrdenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_materiales_ordenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_material', 'codigo_orden', 'cantidad'], 'integer'],
            [['medida'], 'string', 'max' => 100],
            [['codigo_material', 'codigo_orden'], 'unique', 'targetAttribute' => ['codigo_material', 'codigo_orden']],
            [['codigo_proveedor', 'codigo_material'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_material']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_material'], 'exist', 'skipOnError' => true, 'targetClass' => Materiales::className(), 'targetAttribute' => ['codigo_material' => 'codigo_material']],
            [['codigo_orden'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden' => 'codigo_orden_de_fabricacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suministro' => 'Codigo Suministro',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_material' => 'Codigo Material',
            'codigo_orden' => 'Codigo Orden',
            'cantidad' => 'Cantidad',
            'medida' => 'Medida',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoMaterial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaterial()
    {
        return $this->hasOne(Materiales::className(), ['codigo_material' => 'codigo_material']);
    }

    /**
     * Gets query for [[CodigoOrden]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrden()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden']);
    }
}

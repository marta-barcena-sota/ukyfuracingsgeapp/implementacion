<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proyectos".
 *
 * @property int $codigo_proyecto
 * @property int|null $codigo_cliente
 * @property string|null $referencia_interna_proyecto
 * @property string|null $nombre_proyecto
 * @property string|null $carpeta_proyecto
 * @property string|null $referencia_cad_proyecto
 * @property string|null $gantt_proyecto
 * @property string|null $fecha_fin_proyecto
 * @property string|null $fecha_inicio_proyecto
 * @property string|null $tipo_proyecto
 * @property string|null $estado_proyecto
 *
 * @property EmpleadosQueTrabajanEnProyectos[] $empleadosQueTrabajanEnProyectos
 * @property Empleados[] $codigoEmpleados
 * @property IntermediariosQueParticipanEnProyectos[] $intermediariosQueParticipanEnProyectos
 * @property Intermediarios[] $codigoIntermediarios
 * @property OrdenesDeFabricacion[] $ordenesDeFabricacions
 * @property ProductosComponenProyectos[] $productosComponenProyectos
 * @property Productos[] $codigoProductos
 * @property ProveedoresQueParticipanEnProyectos[] $proveedoresQueParticipanEnProyectos
 * @property Proveedores[] $codigoProveedors
 * @property Clientes $codigoCliente
 */
class Proyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['fecha_fin_proyecto', 'fecha_inicio_proyecto'], 'safe'],
            [['referencia_interna_proyecto'], 'string', 'max' => 10],
            [['nombre_proyecto', 'carpeta_proyecto', 'referencia_cad_proyecto', 'gantt_proyecto', 'tipo_proyecto', 'estado_proyecto'], 'string', 'max' => 100],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_proyecto' => 'Codigo Proyecto',
            'codigo_cliente' => 'Codigo Cliente',
            'referencia_interna_proyecto' => 'Referencia Interna Proyecto',
            'nombre_proyecto' => 'Nombre Proyecto',
            'carpeta_proyecto' => 'Carpeta Proyecto',
            'referencia_cad_proyecto' => 'Referencia Cad Proyecto',
            'gantt_proyecto' => 'Gantt Proyecto',
            'fecha_fin_proyecto' => 'Fecha Fin Proyecto',
            'fecha_inicio_proyecto' => 'Fecha Inicio Proyecto',
            'tipo_proyecto' => 'Tipo Proyecto',
            'estado_proyecto' => 'Estado Proyecto',
        ];
    }

    /**
     * Gets query for [[EmpleadosQueTrabajanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadosQueTrabajanEnProyectos()
    {
        return $this->hasMany(EmpleadosQueTrabajanEnProyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_empleado' => 'codigo_empleado'])->viaTable('empleados_que_trabajan_en_proyectos', ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[IntermediariosQueParticipanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIntermediariosQueParticipanEnProyectos()
    {
        return $this->hasMany(IntermediariosQueParticipanEnProyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoIntermediarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediarios()
    {
        return $this->hasMany(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario'])->viaTable('intermediarios_que_participan_en_proyectos', ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[OrdenesDeFabricacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenesDeFabricacions()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[ProductosComponenProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosComponenProyectos()
    {
        return $this->hasMany(ProductosComponenProyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('productos_componen_proyectos', ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[ProveedoresQueParticipanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueParticipanEnProyectos()
    {
        return $this->hasMany(ProveedoresQueParticipanEnProyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_participan_en_proyectos', ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}

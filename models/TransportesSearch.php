<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transportes;

/**
 * TransportesSearch represents the model behind the search form of `app\models\Transportes`.
 */
class TransportesSearch extends Transportes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_transporte', 'codigo_proveedor'], 'integer'],
            [['fecha_llegada', 'fecha_estimada_llegada', 'fecha_envio_transporte', 'tipo_transporte'], 'safe'],
            [['base_imponible_envio'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transportes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_transporte' => $this->codigo_transporte,
            'codigo_proveedor' => $this->codigo_proveedor,
            'fecha_llegada' => $this->fecha_llegada,
            'fecha_estimada_llegada' => $this->fecha_estimada_llegada,
            'base_imponible_envio' => $this->base_imponible_envio,
            'fecha_envio_transporte' => $this->fecha_envio_transporte,
        ]);

        $query->andFilterWhere(['like', 'tipo_transporte', $this->tipo_transporte]);

        return $dataProvider;
    }
}

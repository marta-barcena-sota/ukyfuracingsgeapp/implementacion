<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DireccionesFacturacionClientes;

/**
 * DireccionesFacturacionClientesSearch represents the model behind the search form of `app\models\DireccionesFacturacionClientes`.
 */
class DireccionesFacturacionClientesSearch extends DireccionesFacturacionClientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_direccion_facturacion', 'codigo_cliente'], 'integer'],
            [['direccion_facturacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DireccionesFacturacionClientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_direccion_facturacion' => $this->codigo_direccion_facturacion,
            'codigo_cliente' => $this->codigo_cliente,
        ]);

        $query->andFilterWhere(['like', 'direccion_facturacion', $this->direccion_facturacion]);

        return $dataProvider;
    }
}

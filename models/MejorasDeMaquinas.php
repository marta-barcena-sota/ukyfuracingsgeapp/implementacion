<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mejoras_de_maquinas".
 *
 * @property int $codigo_aplicacion
 * @property int|null $codigo_maquina
 * @property int|null $codigo_mejora
 *
 * @property Maquinas $codigoMaquina
 * @property Mejoras $codigoMejora
 */
class MejorasDeMaquinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mejoras_de_maquinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_maquina', 'codigo_mejora'], 'integer'],
            [['codigo_maquina', 'codigo_mejora'], 'unique', 'targetAttribute' => ['codigo_maquina', 'codigo_mejora']],
            [['codigo_maquina'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::className(), 'targetAttribute' => ['codigo_maquina' => 'codigo_maquina']],
            [['codigo_mejora'], 'exist', 'skipOnError' => true, 'targetClass' => Mejoras::className(), 'targetAttribute' => ['codigo_mejora' => 'codigo_mejora']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_aplicacion' => 'Codigo Aplicacion',
            'codigo_maquina' => 'Codigo Maquina',
            'codigo_mejora' => 'Codigo Mejora',
        ];
    }

    /**
     * Gets query for [[CodigoMaquina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquina()
    {
        return $this->hasOne(Maquinas::className(), ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[CodigoMejora]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMejora()
    {
        return $this->hasOne(Mejoras::className(), ['codigo_mejora' => 'codigo_mejora']);
    }
}

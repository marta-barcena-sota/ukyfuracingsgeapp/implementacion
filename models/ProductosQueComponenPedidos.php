<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_que_componen_pedidos".
 *
 * @property int $codigo_componente
 * @property int|null $codigo_producto
 * @property int|null $codigo_pedido
 * @property int|null $cantidad
 * @property int|null $cantidad_pendiente
 * @property int|null $realizado
 *
 * @property Productos $codigoProducto
 * @property Pedidos $codigoPedido
 */
class ProductosQueComponenPedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_que_componen_pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_producto', 'codigo_pedido', 'cantidad', 'cantidad_pendiente', 'realizado'], 'integer'],
            [['codigo_producto', 'codigo_pedido'], 'unique', 'targetAttribute' => ['codigo_producto', 'codigo_pedido']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
            [['codigo_pedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidos::className(), 'targetAttribute' => ['codigo_pedido' => 'codigo_pedido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_componente' => 'Codigo Componente',
            'codigo_producto' => 'Codigo Producto',
            'codigo_pedido' => 'Codigo Pedido',
            'cantidad' => 'Cantidad',
            'cantidad_pendiente' => 'Cantidad Pendiente',
            'realizado' => 'Realizado',
        ];
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoPedido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedido()
    {
        return $this->hasOne(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }
}

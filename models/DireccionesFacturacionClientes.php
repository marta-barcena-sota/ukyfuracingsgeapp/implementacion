<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "direcciones_facturacion_clientes".
 *
 * @property int $codigo_direccion_facturacion
 * @property int|null $codigo_cliente
 * @property string|null $direccion_facturacion
 *
 * @property Clientes $codigoCliente
 */
class DireccionesFacturacionClientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direcciones_facturacion_clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['direccion_facturacion'], 'string', 'max' => 100],
            [['codigo_cliente', 'direccion_facturacion'], 'unique', 'targetAttribute' => ['codigo_cliente', 'direccion_facturacion']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_direccion_facturacion' => 'Codigo Direccion Facturacion',
            'codigo_cliente' => 'Codigo Cliente',
            'direccion_facturacion' => 'Direccion Facturacion',
        ];
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}

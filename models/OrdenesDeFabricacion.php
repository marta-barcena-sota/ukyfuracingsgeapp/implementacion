<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordenes_de_fabricacion".
 *
 * @property int $codigo_orden_de_fabricacion
 * @property int|null $codigo_empleado
 * @property int|null $codigo_proyecto
 * @property string|null $nombre_orden_de_fabricacion
 * @property string|null $imagen
 * @property string|null $estado
 * @property string|null $fecha_inicio_orden_de_fabricacion
 * @property string|null $fecha_fin_orden_de_fabricacion
 * @property string|null $tipo_orden_de_fabricacion
 * @property string|null $fecha_estimada_fin_orden_fabricacion
 * @property string|null $urgencia_orden_fabricacion
 * @property float|null $estimacion_coste_material
 * @property float|null $coste_fabricacion
 * @property float|null $coste_transporte
 * @property float|null $coste_postprocesado
 * @property float|null $coste_maquina
 * @property float|null $coste_mano_de_obra
 * @property float|null $coste_material
 *
 * @property MaquinasParticipanOrdenes[] $maquinasParticipanOrdenes
 * @property Maquinas[] $codigoMaquinas
 * @property Empleados $codigoEmpleado
 * @property Proyectos $codigoProyecto
 * @property ProductosQueComponenOrdenes[] $productosQueComponenOrdenes
 * @property Productos[] $codigoProductos
 * @property ProveedoresMaterialesOrdenes[] $proveedoresMaterialesOrdenes
 * @property Materiales[] $codigoMaterials
 * @property ProveedoresQueColaboranEnOrdenes[] $proveedoresQueColaboranEnOrdenes
 * @property Proveedores[] $codigoProveedors
 * @property ProveedoresQueSuministranPostprocesadosAOrdenes[] $proveedoresQueSuministranPostprocesadosAOrdenes
 * @property Postprocesados[] $codigoPostprocesados
 * @property Proveedores[] $codigoProveedors0
 * @property Seguimientos[] $seguimientos
 */
class OrdenesDeFabricacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordenes_de_fabricacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_proyecto'], 'integer'],
            [['fecha_inicio_orden_de_fabricacion', 'fecha_fin_orden_de_fabricacion', 'fecha_estimada_fin_orden_fabricacion'], 'safe'],
            [['estimacion_coste_material', 'coste_fabricacion', 'coste_transporte', 'coste_postprocesado', 'coste_maquina', 'coste_mano_de_obra', 'coste_material'], 'number'],
            [['nombre_orden_de_fabricacion', 'imagen', 'estado', 'tipo_orden_de_fabricacion', 'urgencia_orden_fabricacion'], 'string', 'max' => 100],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['codigo_proyecto' => 'codigo_proyecto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_orden_de_fabricacion' => 'Codigo Orden De Fabricacion',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_proyecto' => 'Codigo Proyecto',
            'nombre_orden_de_fabricacion' => 'Nombre Orden De Fabricacion',
            'imagen' => 'Imagen',
            'estado' => 'Estado',
            'fecha_inicio_orden_de_fabricacion' => 'Fecha Inicio Orden De Fabricacion',
            'fecha_fin_orden_de_fabricacion' => 'Fecha Fin Orden De Fabricacion',
            'tipo_orden_de_fabricacion' => 'Tipo Orden De Fabricacion',
            'fecha_estimada_fin_orden_fabricacion' => 'Fecha Estimada Fin Orden Fabricacion',
            'urgencia_orden_fabricacion' => 'Urgencia Orden Fabricacion',
            'estimacion_coste_material' => 'Estimacion Coste Material',
            'coste_fabricacion' => 'Coste Fabricacion',
            'coste_transporte' => 'Coste Transporte',
            'coste_postprocesado' => 'Coste Postprocesado',
            'coste_maquina' => 'Coste Maquina',
            'coste_mano_de_obra' => 'Coste Mano De Obra',
            'coste_material' => 'Coste Material',
        ];
    }

    /**
     * Gets query for [[MaquinasParticipanOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinasParticipanOrdenes()
    {
        return $this->hasMany(MaquinasParticipanOrdenes::className(), ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquinas()
    {
        return $this->hasMany(Maquinas::className(), ['codigo_maquina' => 'codigo_maquina'])->viaTable('maquinas_participan_ordenes', ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoProyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[ProductosQueComponenOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenOrdenes()
    {
        return $this->hasMany(ProductosQueComponenOrdenes::className(), ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('productos_que_componen_ordenes', ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[ProveedoresMaterialesOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresMaterialesOrdenes()
    {
        return $this->hasMany(ProveedoresMaterialesOrdenes::className(), ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoMaterials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaterials()
    {
        return $this->hasMany(Materiales::className(), ['codigo_material' => 'codigo_material'])->viaTable('proveedores_materiales_ordenes', ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[ProveedoresQueColaboranEnOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueColaboranEnOrdenes()
    {
        return $this->hasMany(ProveedoresQueColaboranEnOrdenes::className(), ['codigo_orden_fabricacion' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_colaboran_en_ordenes', ['codigo_orden_fabricacion' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranPostprocesadosAOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranPostprocesadosAOrdenes()
    {
        return $this->hasMany(ProveedoresQueSuministranPostprocesadosAOrdenes::className(), ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoPostprocesados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostprocesados()
    {
        return $this->hasMany(Postprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoProveedors0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors0()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_orden' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[Seguimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeguimientos()
    {
        return $this->hasMany(Seguimientos::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden_de_fabricacion']);
    }
}

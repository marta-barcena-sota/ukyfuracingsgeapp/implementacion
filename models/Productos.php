<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $codigo_producto
 * @property int|null $cantidad_en_stock
 * @property string|null $forma_de_almacenamiento
 * @property string|null $concepto_producto
 * @property string|null $primera_categoria_producto
 * @property string|null $segunda_categoria_producto
 * @property string|null $tercera_categoria_producto
 * @property string|null $referencia_interna_producto
 * @property string|null $referencia_articulo_producto
 *
 * @property AlmacenesQueGuardanProductos[] $almacenesQueGuardanProductos
 * @property Almacenes[] $codigoAlmacens
 * @property Comentarios[] $comentarios
 * @property Compras[] $compras
 * @property Empleados[] $codigoEmpleados
 * @property Documentaciones[] $documentaciones
 * @property ProductosComponenProyectos[] $productosComponenProyectos
 * @property Proyectos[] $codigoProyectos
 * @property ProductosQueComponenOrdenes[] $productosQueComponenOrdenes
 * @property OrdenesDeFabricacion[] $codigoOrdens
 * @property ProductosQueComponenPedidos[] $productosQueComponenPedidos
 * @property Pedidos[] $codigoPedidos
 * @property ProductosQueComponenProductos[] $productosQueComponenProductos
 * @property ProductosQueComponenProductos[] $productosQueComponenProductos0
 * @property Productos[] $acabados
 * @property Productos[] $semielaborados
 * @property ProductosQueSufrenCambios[] $productosQueSufrenCambios
 * @property Cambios[] $codigoCambios
 * @property ProveedoresQueSuministranProductos[] $proveedoresQueSuministranProductos
 * @property Proveedores[] $codigoProveedors
 * @property Ventas[] $ventas
 * @property Clientes[] $codClientes
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad_en_stock'], 'integer'],
            [['forma_de_almacenamiento', 'concepto_producto', 'primera_categoria_producto', 'segunda_categoria_producto', 'tercera_categoria_producto', 'referencia_articulo_producto'], 'string', 'max' => 100],
            [['referencia_interna_producto'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_producto' => 'Codigo Producto',
            'cantidad_en_stock' => 'Cantidad En Stock',
            'forma_de_almacenamiento' => 'Forma De Almacenamiento',
            'concepto_producto' => 'Concepto Producto',
            'primera_categoria_producto' => 'Primera Categoria Producto',
            'segunda_categoria_producto' => 'Segunda Categoria Producto',
            'tercera_categoria_producto' => 'Tercera Categoria Producto',
            'referencia_interna_producto' => 'Referencia Interna Producto',
            'referencia_articulo_producto' => 'Referencia Articulo Producto',
        ];
    }

    /**
     * Gets query for [[AlmacenesQueGuardanProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacenesQueGuardanProductos()
    {
        return $this->hasMany(AlmacenesQueGuardanProductos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoAlmacens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacens()
    {
        return $this->hasMany(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen'])->viaTable('almacenes_que_guardan_productos', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_empleado' => 'codigo_empleado'])->viaTable('compras', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Documentaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentaciones()
    {
        return $this->hasMany(Documentaciones::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosComponenProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosComponenProyectos()
    {
        return $this->hasMany(ProductosComponenProyectos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto'])->viaTable('productos_componen_proyectos', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosQueComponenOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenOrdenes()
    {
        return $this->hasMany(ProductosQueComponenOrdenes::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoOrdens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdens()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden'])->viaTable('productos_que_componen_ordenes', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosQueComponenPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenPedidos()
    {
        return $this->hasMany(ProductosQueComponenPedidos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido'])->viaTable('productos_que_componen_pedidos', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosQueComponenProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenProductos()
    {
        return $this->hasMany(ProductosQueComponenProductos::className(), ['semielaborado' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosQueComponenProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenProductos0()
    {
        return $this->hasMany(ProductosQueComponenProductos::className(), ['acabado' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Acabados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcabados()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'acabado'])->viaTable('productos_que_componen_productos', ['semielaborado' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Semielaborados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemielaborados()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'semielaborado'])->viaTable('productos_que_componen_productos', ['acabado' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProductosQueSufrenCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueSufrenCambios()
    {
        return $this->hasMany(ProductosQueSufrenCambios::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCambios()
    {
        return $this->hasMany(Cambios::className(), ['codigo_cambio' => 'codigo_cambio'])->viaTable('productos_que_sufren_cambios', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranProductos()
    {
        return $this->hasMany(ProveedoresQueSuministranProductos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_suministran_productos', ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['cod_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodClientes()
    {
        return $this->hasMany(Clientes::className(), ['codigo_cliente' => 'cod_cliente'])->viaTable('ventas', ['cod_producto' => 'codigo_producto']);
    }
}

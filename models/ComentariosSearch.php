<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Comentarios;

/**
 * ComentariosSearch represents the model behind the search form of `app\models\Comentarios`.
 */
class ComentariosSearch extends Comentarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_comentario', 'codigo_empleado', 'codigo_seguimiento', 'codigo_proveedor', 'codigo_pedido', 'codigo_producto', 'codigo_cliente', 'codigo_cambio', 'codigo_intermediario'], 'integer'],
            [['concepto_comentario', 'fecha_comentario', 'hora_comentario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comentarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_comentario' => $this->codigo_comentario,
            'codigo_empleado' => $this->codigo_empleado,
            'codigo_seguimiento' => $this->codigo_seguimiento,
            'codigo_proveedor' => $this->codigo_proveedor,
            'codigo_pedido' => $this->codigo_pedido,
            'codigo_producto' => $this->codigo_producto,
            'codigo_cliente' => $this->codigo_cliente,
            'codigo_cambio' => $this->codigo_cambio,
            'codigo_intermediario' => $this->codigo_intermediario,
            'fecha_comentario' => $this->fecha_comentario,
            'hora_comentario' => $this->hora_comentario,
        ]);

        $query->andFilterWhere(['like', 'concepto_comentario', $this->concepto_comentario]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Intermediarios;

/**
 * IntermediariosSearch represents the model behind the search form of `app\models\Intermediarios`.
 */
class IntermediariosSearch extends Intermediarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_intermediario'], 'integer'],
            [['eori_intermediario', 'cif_intermediario', 'nombre_intermediario', 'tipo_empresa_intermediario', 'sociedad_empresa_intermediario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Intermediarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_intermediario' => $this->codigo_intermediario,
        ]);

        $query->andFilterWhere(['like', 'eori_intermediario', $this->eori_intermediario])
            ->andFilterWhere(['like', 'cif_intermediario', $this->cif_intermediario])
            ->andFilterWhere(['like', 'nombre_intermediario', $this->nombre_intermediario])
            ->andFilterWhere(['like', 'tipo_empresa_intermediario', $this->tipo_empresa_intermediario])
            ->andFilterWhere(['like', 'sociedad_empresa_intermediario', $this->sociedad_empresa_intermediario]);

        return $dataProvider;
    }
}

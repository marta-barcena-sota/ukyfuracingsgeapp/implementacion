<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "intermediarios".
 *
 * @property int $codigo_intermediario
 * @property string|null $eori_intermediario
 * @property string|null $cif_intermediario
 * @property string|null $nombre_intermediario
 * @property string|null $tipo_empresa_intermediario
 * @property string|null $sociedad_empresa_intermediario
 *
 * @property Comentarios[] $comentarios
 * @property EmailsIntermediarios[] $emailsIntermediarios
 * @property EnlacesIntermediarios[] $enlacesIntermediarios
 * @property IntermediariosQueParticipanEnProyectos[] $intermediariosQueParticipanEnProyectos
 * @property Proyectos[] $codigoProyectos
 * @property TlfsIntermediarios[] $tlfsIntermediarios
 */
class Intermediarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'intermediarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eori_intermediario'], 'string', 'max' => 50],
            [['cif_intermediario'], 'string', 'max' => 9],
            [['nombre_intermediario', 'tipo_empresa_intermediario', 'sociedad_empresa_intermediario'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_intermediario' => 'Codigo Intermediario',
            'eori_intermediario' => 'Eori Intermediario',
            'cif_intermediario' => 'Cif Intermediario',
            'nombre_intermediario' => 'Nombre Intermediario',
            'tipo_empresa_intermediario' => 'Tipo Empresa Intermediario',
            'sociedad_empresa_intermediario' => 'Sociedad Empresa Intermediario',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[EmailsIntermediarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmailsIntermediarios()
    {
        return $this->hasMany(EmailsIntermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[EnlacesIntermediarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEnlacesIntermediarios()
    {
        return $this->hasMany(EnlacesIntermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[IntermediariosQueParticipanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIntermediariosQueParticipanEnProyectos()
    {
        return $this->hasMany(IntermediariosQueParticipanEnProyectos::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[CodigoProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto'])->viaTable('intermediarios_que_participan_en_proyectos', ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[TlfsIntermediarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTlfsIntermediarios()
    {
        return $this->hasMany(TlfsIntermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $codigo_venta
 * @property int|null $cod_cliente
 * @property int|null $cod_producto
 * @property int|null $cantidad
 * @property float|null $base_imponible_unitaria_venta
 * @property float|null $beneficio
 * @property string|null $fecha_de_salida
 * @property string|null $referencia_interna_venta
 *
 * @property Clientes $codCliente
 * @property Productos $codProducto
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_cliente', 'cod_producto', 'cantidad'], 'integer'],
            [['base_imponible_unitaria_venta', 'beneficio'], 'number'],
            [['fecha_de_salida'], 'safe'],
            [['referencia_interna_venta'], 'string', 'max' => 6],
            [['cod_cliente', 'cod_producto'], 'unique', 'targetAttribute' => ['cod_cliente', 'cod_producto']],
            [['cod_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cod_cliente' => 'codigo_cliente']],
            [['cod_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['cod_producto' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_venta' => 'Codigo Venta',
            'cod_cliente' => 'Cod Cliente',
            'cod_producto' => 'Cod Producto',
            'cantidad' => 'Cantidad',
            'base_imponible_unitaria_venta' => 'Base Imponible Unitaria Venta',
            'beneficio' => 'Beneficio',
            'fecha_de_salida' => 'Fecha De Salida',
            'referencia_interna_venta' => 'Referencia Interna Venta',
        ];
    }

    /**
     * Gets query for [[CodCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'cod_cliente']);
    }

    /**
     * Gets query for [[CodProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'cod_producto']);
    }
}

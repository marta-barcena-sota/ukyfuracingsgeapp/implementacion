<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "direcciones_envio_clientes".
 *
 * @property int $codigo_direccion_envio
 * @property int|null $codigo_cliente
 * @property string|null $direccion_envio
 *
 * @property Clientes $codigoCliente
 */
class DireccionesEnvioClientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direcciones_envio_clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['direccion_envio'], 'string', 'max' => 100],
            [['codigo_cliente', 'direccion_envio'], 'unique', 'targetAttribute' => ['codigo_cliente', 'direccion_envio']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_direccion_envio' => 'Codigo Direccion Envio',
            'codigo_cliente' => 'Codigo Cliente',
            'direccion_envio' => 'Direccion Envio',
        ];
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}

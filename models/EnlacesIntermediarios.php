<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enlaces_intermediarios".
 *
 * @property int $codigo_enlace
 * @property int|null $codigo_intermediario
 * @property string|null $enlace_intermediario
 *
 * @property Intermediarios $codigoIntermediario
 */
class EnlacesIntermediarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enlaces_intermediarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_intermediario'], 'integer'],
            [['enlace_intermediario'], 'string', 'max' => 100],
            [['codigo_intermediario', 'enlace_intermediario'], 'unique', 'targetAttribute' => ['codigo_intermediario', 'enlace_intermediario']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_enlace' => 'Codigo Enlace',
            'codigo_intermediario' => 'Codigo Intermediario',
            'enlace_intermediario' => 'Enlace Intermediario',
        ];
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}

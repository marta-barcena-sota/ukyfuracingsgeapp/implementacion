<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas".
 *
 * @property int $codigo_maquina
 * @property string|null $nombre_maquina
 * @property string|null $numero_de_serie_maquina
 * @property string|null $software_maquina
 * @property string|null $manual_mantenimiento_maquina
 * @property string|null $modelo_maquina
 * @property string|null $manual_procedimiento_maquina
 * @property string|null $carpeta_maquina
 * @property string|null $tipo_maquina
 * @property string|null $descripcion_maquina
 * @property string|null $manual_maquina
 * @property string|null $firmware_maquina
 * @property string|null $fabricante_maquina
 *
 * @property Mantenimientos[] $mantenimientos
 * @property MaquinasParticipanOrdenes[] $maquinasParticipanOrdenes
 * @property OrdenesDeFabricacion[] $codigoOrdens
 * @property MejorasDeMaquinas[] $mejorasDeMaquinas
 * @property Mejoras[] $codigoMejoras
 */
class Maquinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_maquina', 'numero_de_serie_maquina', 'software_maquina', 'manual_mantenimiento_maquina', 'modelo_maquina', 'manual_procedimiento_maquina', 'carpeta_maquina', 'tipo_maquina', 'descripcion_maquina', 'manual_maquina', 'firmware_maquina', 'fabricante_maquina'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_maquina' => 'Codigo Maquina',
            'nombre_maquina' => 'Nombre Maquina',
            'numero_de_serie_maquina' => 'Numero De Serie Maquina',
            'software_maquina' => 'Software Maquina',
            'manual_mantenimiento_maquina' => 'Manual Mantenimiento Maquina',
            'modelo_maquina' => 'Modelo Maquina',
            'manual_procedimiento_maquina' => 'Manual Procedimiento Maquina',
            'carpeta_maquina' => 'Carpeta Maquina',
            'tipo_maquina' => 'Tipo Maquina',
            'descripcion_maquina' => 'Descripcion Maquina',
            'manual_maquina' => 'Manual Maquina',
            'firmware_maquina' => 'Firmware Maquina',
            'fabricante_maquina' => 'Fabricante Maquina',
        ];
    }

    /**
     * Gets query for [[Mantenimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientos()
    {
        return $this->hasMany(Mantenimientos::className(), ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[MaquinasParticipanOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinasParticipanOrdenes()
    {
        return $this->hasMany(MaquinasParticipanOrdenes::className(), ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[CodigoOrdens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdens()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden'])->viaTable('maquinas_participan_ordenes', ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[MejorasDeMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMejorasDeMaquinas()
    {
        return $this->hasMany(MejorasDeMaquinas::className(), ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[CodigoMejoras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMejoras()
    {
        return $this->hasMany(Mejoras::className(), ['codigo_mejora' => 'codigo_mejora'])->viaTable('mejoras_de_maquinas', ['codigo_maquina' => 'codigo_maquina']);
    }
}

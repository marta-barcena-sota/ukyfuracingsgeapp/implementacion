<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proveedores;

/**
 * ProveedoresSearch represents the model behind the search form of `app\models\Proveedores`.
 */
class ProveedoresSearch extends Proveedores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor'], 'integer'],
            [['cif_proveedor', 'eori_proveedor', 'nombre_proveedor', 'tipo_de_empresa_proveedor', 'sociedad_empresa_proveedor', 'pais_proveedor', 'tipo_proveedor'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proveedores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_proveedor' => $this->codigo_proveedor,
        ]);

        $query->andFilterWhere(['like', 'cif_proveedor', $this->cif_proveedor])
            ->andFilterWhere(['like', 'eori_proveedor', $this->eori_proveedor])
            ->andFilterWhere(['like', 'nombre_proveedor', $this->nombre_proveedor])
            ->andFilterWhere(['like', 'tipo_de_empresa_proveedor', $this->tipo_de_empresa_proveedor])
            ->andFilterWhere(['like', 'sociedad_empresa_proveedor', $this->sociedad_empresa_proveedor])
            ->andFilterWhere(['like', 'pais_proveedor', $this->pais_proveedor])
            ->andFilterWhere(['like', 'tipo_proveedor', $this->tipo_proveedor]);

        return $dataProvider;
    }
}

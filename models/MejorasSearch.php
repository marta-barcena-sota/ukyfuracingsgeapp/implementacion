<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mejoras;

/**
 * MejorasSearch represents the model behind the search form of `app\models\Mejoras`.
 */
class MejorasSearch extends Mejoras
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_mejora', 'codigo_empleado'], 'integer'],
            [['descripcion_mejora', 'carpeta_mejora', 'fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mejoras::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_mejora' => $this->codigo_mejora,
            'codigo_empleado' => $this->codigo_empleado,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'descripcion_mejora', $this->descripcion_mejora])
            ->andFilterWhere(['like', 'carpeta_mejora', $this->carpeta_mejora]);

        return $dataProvider;
    }
}

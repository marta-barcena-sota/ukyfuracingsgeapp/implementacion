<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_que_sufren_cambios".
 *
 * @property int $codigo_traspaso
 * @property int|null $codigo_producto
 * @property int|null $codigo_cambio
 * @property int|null $cantidad
 *
 * @property Productos $codigoProducto
 * @property Cambios $codigoCambio
 */
class ProductosQueSufrenCambios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_que_sufren_cambios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_producto', 'codigo_cambio', 'cantidad'], 'integer'],
            [['codigo_producto', 'codigo_cambio'], 'unique', 'targetAttribute' => ['codigo_producto', 'codigo_cambio']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
            [['codigo_cambio'], 'exist', 'skipOnError' => true, 'targetClass' => Cambios::className(), 'targetAttribute' => ['codigo_cambio' => 'codigo_cambio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_traspaso' => 'Codigo Traspaso',
            'codigo_producto' => 'Codigo Producto',
            'codigo_cambio' => 'Codigo Cambio',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoCambio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCambio()
    {
        return $this->hasOne(Cambios::className(), ['codigo_cambio' => 'codigo_cambio']);
    }
}

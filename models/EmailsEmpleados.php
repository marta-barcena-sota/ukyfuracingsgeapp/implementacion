<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emails_empleados".
 *
 * @property int $codigo_email
 * @property int|null $codigo_empleado
 * @property string|null $email_empleado
 *
 * @property Empleados $codigoEmpleado
 */
class EmailsEmpleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails_empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado'], 'integer'],
            [['email_empleado'], 'string', 'max' => 100],
            [['codigo_empleado', 'email_empleado'], 'unique', 'targetAttribute' => ['codigo_empleado', 'email_empleado']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_email' => 'Codigo Email',
            'codigo_empleado' => 'Codigo Empleado',
            'email_empleado' => 'Email Empleado',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }
}

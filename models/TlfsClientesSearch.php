<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TlfsClientes;

/**
 * TlfsClientesSearch represents the model behind the search form of `app\models\TlfsClientes`.
 */
class TlfsClientesSearch extends TlfsClientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_tlf', 'codigo_cliente'], 'integer'],
            [['tlf_cliente'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TlfsClientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_tlf' => $this->codigo_tlf,
            'codigo_cliente' => $this->codigo_cliente,
        ]);

        $query->andFilterWhere(['like', 'tlf_cliente', $this->tlf_cliente]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materiales".
 *
 * @property int $codigo_material
 * @property string|null $familia_material
 * @property string|null $clase_material
 * @property string|null $tipo_material
 * @property string|null $carpeta_material
 *
 * @property ProveedoresMaterialesOrdenes[] $proveedoresMaterialesOrdenes
 * @property Proveedores[] $codigoProveedors
 * @property OrdenesDeFabricacion[] $codigoOrdens
 * @property ProveedoresQueSuministranMateriales[] $proveedoresQueSuministranMateriales
 * @property Proveedores[] $codigoProveedors0
 */
class Materiales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['familia_material', 'clase_material', 'tipo_material', 'carpeta_material'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_material' => 'Codigo Material',
            'familia_material' => 'Familia Material',
            'clase_material' => 'Clase Material',
            'tipo_material' => 'Tipo Material',
            'carpeta_material' => 'Carpeta Material',
        ];
    }

    /**
     * Gets query for [[ProveedoresMaterialesOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresMaterialesOrdenes()
    {
        return $this->hasMany(ProveedoresMaterialesOrdenes::className(), ['codigo_material' => 'codigo_material']);
    }

    /**
     * Gets query for [[CodigoProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_materiales_ordenes', ['codigo_material' => 'codigo_material']);
    }

    /**
     * Gets query for [[CodigoOrdens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdens()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden'])->viaTable('proveedores_materiales_ordenes', ['codigo_material' => 'codigo_material']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranMateriales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranMateriales()
    {
        return $this->hasMany(ProveedoresQueSuministranMateriales::className(), ['codigo_material' => 'codigo_material']);
    }

    /**
     * Gets query for [[CodigoProveedors0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors0()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_suministran_materiales', ['codigo_material' => 'codigo_material']);
    }
}

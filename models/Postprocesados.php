<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "postprocesados".
 *
 * @property int $codigo_postprocesado
 * @property string|null $fecha_inicio_postprocesado
 * @property string|null $fecha_fin_postprocesado
 * @property string|null $descripcion_postprocesado
 *
 * @property ProveedoresQueSuministranPostprocesados[] $proveedoresQueSuministranPostprocesados
 * @property Proveedores[] $codigoProveedors
 * @property ProveedoresQueSuministranPostprocesadosAOrdenes[] $proveedoresQueSuministranPostprocesadosAOrdenes
 * @property Proveedores[] $codigoProveedors0
 * @property OrdenesDeFabricacion[] $codigoOrdens
 */
class Postprocesados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'postprocesados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio_postprocesado', 'fecha_fin_postprocesado'], 'safe'],
            [['descripcion_postprocesado'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_postprocesado' => 'Codigo Postprocesado',
            'fecha_inicio_postprocesado' => 'Fecha Inicio Postprocesado',
            'fecha_fin_postprocesado' => 'Fecha Fin Postprocesado',
            'descripcion_postprocesado' => 'Descripcion Postprocesado',
        ];
    }

    /**
     * Gets query for [[ProveedoresQueSuministranPostprocesados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranPostprocesados()
    {
        return $this->hasMany(ProveedoresQueSuministranPostprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado']);
    }

    /**
     * Gets query for [[CodigoProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_suministran_postprocesados', ['codigo_postprocesado' => 'codigo_postprocesado']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranPostprocesadosAOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranPostprocesadosAOrdenes()
    {
        return $this->hasMany(ProveedoresQueSuministranPostprocesadosAOrdenes::className(), ['codigo_postprocesado' => 'codigo_postprocesado']);
    }

    /**
     * Gets query for [[CodigoProveedors0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedors0()
    {
        return $this->hasMany(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_postprocesado' => 'codigo_postprocesado']);
    }

    /**
     * Gets query for [[CodigoOrdens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdens()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_postprocesado' => 'codigo_postprocesado']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_colaboran_en_ordenes".
 *
 * @property int $codigo_colaboracion
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_orden_fabricacion
 *
 * @property Proveedores $codigoProveedor
 * @property OrdenesDeFabricacion $codigoOrdenFabricacion
 */
class ProveedoresQueColaboranEnOrdenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_colaboran_en_ordenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_orden_fabricacion'], 'integer'],
            [['codigo_proveedor', 'codigo_orden_fabricacion'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_orden_fabricacion']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_orden_fabricacion'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden_fabricacion' => 'codigo_orden_de_fabricacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_colaboracion' => 'Codigo Colaboracion',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_orden_fabricacion' => 'Codigo Orden Fabricacion',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoOrdenFabricacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdenFabricacion()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden_fabricacion']);
    }
}

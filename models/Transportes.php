<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transportes".
 *
 * @property int $codigo_transporte
 * @property int|null $codigo_proveedor
 * @property string|null $fecha_llegada
 * @property string|null $fecha_estimada_llegada
 * @property float|null $base_imponible_envio
 * @property string|null $fecha_envio_transporte
 * @property string|null $tipo_transporte
 *
 * @property Proveedores $codigoProveedor
 * @property TransportesQueSufrenLosPedidos[] $transportesQueSufrenLosPedidos
 * @property Pedidos[] $codigoPedidos
 */
class Transportes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transportes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor'], 'integer'],
            [['fecha_llegada', 'fecha_estimada_llegada', 'fecha_envio_transporte'], 'safe'],
            [['base_imponible_envio'], 'number'],
            [['tipo_transporte'], 'string', 'max' => 100],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_transporte' => 'Codigo Transporte',
            'codigo_proveedor' => 'Codigo Proveedor',
            'fecha_llegada' => 'Fecha Llegada',
            'fecha_estimada_llegada' => 'Fecha Estimada Llegada',
            'base_imponible_envio' => 'Base Imponible Envio',
            'fecha_envio_transporte' => 'Fecha Envio Transporte',
            'tipo_transporte' => 'Tipo Transporte',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[TransportesQueSufrenLosPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransportesQueSufrenLosPedidos()
    {
        return $this->hasMany(TransportesQueSufrenLosPedidos::className(), ['codigo_transporte' => 'codigo_transporte']);
    }

    /**
     * Gets query for [[CodigoPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido'])->viaTable('transportes_que_sufren_los_pedidos', ['codigo_transporte' => 'codigo_transporte']);
    }
}

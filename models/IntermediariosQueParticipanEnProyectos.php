<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "intermediarios_que_participan_en_proyectos".
 *
 * @property int $codigo_participacion
 * @property int|null $codigo_intermediario
 * @property int|null $codigo_proyecto
 *
 * @property Intermediarios $codigoIntermediario
 * @property Proyectos $codigoProyecto
 */
class IntermediariosQueParticipanEnProyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'intermediarios_que_participan_en_proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_intermediario', 'codigo_proyecto'], 'integer'],
            [['codigo_intermediario', 'codigo_proyecto'], 'unique', 'targetAttribute' => ['codigo_intermediario', 'codigo_proyecto']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
            [['codigo_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['codigo_proyecto' => 'codigo_proyecto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_participacion' => 'Codigo Participacion',
            'codigo_intermediario' => 'Codigo Intermediario',
            'codigo_proyecto' => 'Codigo Proyecto',
        ];
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }

    /**
     * Gets query for [[CodigoProyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Compras;

/**
 * ComprasSearch represents the model behind the search form of `app\models\Compras`.
 */
class ComprasSearch extends Compras
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_compra', 'codigo_empleado', 'codigo_producto', 'cantidad_adquirida'], 'integer'],
            [['fecha_adquisicion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Compras::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_compra' => $this->codigo_compra,
            'codigo_empleado' => $this->codigo_empleado,
            'codigo_producto' => $this->codigo_producto,
            'cantidad_adquirida' => $this->cantidad_adquirida,
            'fecha_adquisicion' => $this->fecha_adquisicion,
        ]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "almacenes_que_gestionan_cambios".
 *
 * @property int $codigo_gestion
 * @property int|null $codigo_almacen
 * @property int|null $codigo_cambio
 * @property int|null $origen
 * @property int|null $destino
 *
 * @property Almacenes $codigoAlmacen
 * @property Cambios $codigoCambio
 */
class AlmacenesQueGestionanCambios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'almacenes_que_gestionan_cambios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_almacen', 'codigo_cambio', 'origen', 'destino'], 'integer'],
            [['codigo_almacen', 'codigo_cambio'], 'unique', 'targetAttribute' => ['codigo_almacen', 'codigo_cambio']],
            [['codigo_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::className(), 'targetAttribute' => ['codigo_almacen' => 'codigo_almacen']],
            [['codigo_cambio'], 'exist', 'skipOnError' => true, 'targetClass' => Cambios::className(), 'targetAttribute' => ['codigo_cambio' => 'codigo_cambio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_gestion' => 'Codigo Gestion',
            'codigo_almacen' => 'Codigo Almacen',
            'codigo_cambio' => 'Codigo Cambio',
            'origen' => 'Origen',
            'destino' => 'Destino',
        ];
    }

    /**
     * Gets query for [[CodigoAlmacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacen()
    {
        return $this->hasOne(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[CodigoCambio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCambio()
    {
        return $this->hasOne(Cambios::className(), ['codigo_cambio' => 'codigo_cambio']);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Almacenes;

/**
 * AlmacenesSearch represents the model behind the search form of `app\models\Almacenes`.
 */
class AlmacenesSearch extends Almacenes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_almacen'], 'integer'],
            [['denominacion_almacen', 'direccion_almacen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Almacenes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_almacen' => $this->codigo_almacen,
        ]);

        $query->andFilterWhere(['like', 'denominacion_almacen', $this->denominacion_almacen])
            ->andFilterWhere(['like', 'direccion_almacen', $this->direccion_almacen]);

        return $dataProvider;
    }
}

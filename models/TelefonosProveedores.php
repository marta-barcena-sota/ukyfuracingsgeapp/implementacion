<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos_proveedores".
 *
 * @property int $codigo_tlf_proveedor
 * @property int|null $codigo_proveedor
 * @property string|null $tlf_proveedor
 *
 * @property Proveedores $codigoProveedor
 */
class TelefonosProveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos_proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor'], 'integer'],
            [['tlf_proveedor'], 'string', 'max' => 100],
            [['codigo_proveedor', 'tlf_proveedor'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'tlf_proveedor']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tlf_proveedor' => 'Codigo Tlf Proveedor',
            'codigo_proveedor' => 'Codigo Proveedor',
            'tlf_proveedor' => 'Tlf Proveedor',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transportes_que_sufren_los_pedidos".
 *
 * @property int $codigo_viaje
 * @property int|null $codigo_pedido
 * @property int|null $codigo_transporte
 *
 * @property Pedidos $codigoPedido
 * @property Transportes $codigoTransporte
 */
class TransportesQueSufrenLosPedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transportes_que_sufren_los_pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_pedido', 'codigo_transporte'], 'integer'],
            [['codigo_pedido', 'codigo_transporte'], 'unique', 'targetAttribute' => ['codigo_pedido', 'codigo_transporte']],
            [['codigo_pedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidos::className(), 'targetAttribute' => ['codigo_pedido' => 'codigo_pedido']],
            [['codigo_transporte'], 'exist', 'skipOnError' => true, 'targetClass' => Transportes::className(), 'targetAttribute' => ['codigo_transporte' => 'codigo_transporte']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_viaje' => 'Codigo Viaje',
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_transporte' => 'Codigo Transporte',
        ];
    }

    /**
     * Gets query for [[CodigoPedido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedido()
    {
        return $this->hasOne(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoTransporte]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoTransporte()
    {
        return $this->hasOne(Transportes::className(), ['codigo_transporte' => 'codigo_transporte']);
    }
}

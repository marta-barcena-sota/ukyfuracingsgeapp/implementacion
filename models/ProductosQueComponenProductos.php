<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_que_componen_productos".
 *
 * @property int $codigo_componente
 * @property int|null $semielaborado
 * @property int|null $acabado
 *
 * @property Productos $semielaborado0
 * @property Productos $acabado0
 */
class ProductosQueComponenProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_que_componen_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semielaborado', 'acabado'], 'integer'],
            [['semielaborado', 'acabado'], 'unique', 'targetAttribute' => ['semielaborado', 'acabado']],
            [['semielaborado'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['semielaborado' => 'codigo_producto']],
            [['acabado'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['acabado' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_componente' => 'Codigo Componente',
            'semielaborado' => 'Semielaborado',
            'acabado' => 'Acabado',
        ];
    }

    /**
     * Gets query for [[Semielaborado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSemielaborado0()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'semielaborado']);
    }

    /**
     * Gets query for [[Acabado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcabado0()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'acabado']);
    }
}

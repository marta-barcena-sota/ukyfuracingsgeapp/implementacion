<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tlfs_clientes".
 *
 * @property int $codigo_tlf
 * @property int|null $codigo_cliente
 * @property string|null $tlf_cliente
 *
 * @property Clientes $codigoCliente
 */
class TlfsClientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tlfs_clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['tlf_cliente'], 'string', 'max' => 100],
            [['codigo_cliente', 'tlf_cliente'], 'unique', 'targetAttribute' => ['codigo_cliente', 'tlf_cliente']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tlf' => 'Codigo Tlf',
            'codigo_cliente' => 'Codigo Cliente',
            'tlf_cliente' => 'Tlf Cliente',
        ];
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_suministran_postprocesados_a_ordenes".
 *
 * @property int $codigo_suministro
 * @property int|null $codigo_postprocesado
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_orden
 *
 * @property Postprocesados $codigoPostprocesado
 * @property Proveedores $codigoProveedor
 * @property OrdenesDeFabricacion $codigoOrden
 */
class ProveedoresQueSuministranPostprocesadosAOrdenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_suministran_postprocesados_a_ordenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_postprocesado', 'codigo_proveedor', 'codigo_orden'], 'integer'],
            [['codigo_postprocesado', 'codigo_proveedor'], 'unique', 'targetAttribute' => ['codigo_postprocesado', 'codigo_proveedor']],
            [['codigo_postprocesado', 'codigo_orden'], 'unique', 'targetAttribute' => ['codigo_postprocesado', 'codigo_orden']],
            [['codigo_proveedor', 'codigo_orden'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_orden']],
            [['codigo_postprocesado'], 'exist', 'skipOnError' => true, 'targetClass' => Postprocesados::className(), 'targetAttribute' => ['codigo_postprocesado' => 'codigo_postprocesado']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_orden'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden' => 'codigo_orden_de_fabricacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suministro' => 'Codigo Suministro',
            'codigo_postprocesado' => 'Codigo Postprocesado',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_orden' => 'Codigo Orden',
        ];
    }

    /**
     * Gets query for [[CodigoPostprocesado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostprocesado()
    {
        return $this->hasOne(Postprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado']);
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoOrden]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrden()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_suministran_productos".
 *
 * @property int $codigo_suministro
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_producto
 * @property float|null $base_imponible_unitaria
 * @property float|null $iva_impuesto
 *
 * @property Proveedores $codigoProveedor
 * @property Productos $codigoProducto
 */
class ProveedoresQueSuministranProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_suministran_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_producto'], 'integer'],
            [['base_imponible_unitaria', 'iva_impuesto'], 'number'],
            [['codigo_proveedor', 'codigo_producto'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_producto']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suministro' => 'Codigo Suministro',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_producto' => 'Codigo Producto',
            'base_imponible_unitaria' => 'Base Imponible Unitaria',
            'iva_impuesto' => 'Iva Impuesto',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }
}

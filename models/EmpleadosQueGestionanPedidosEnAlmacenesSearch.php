<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpleadosQueGestionanPedidosEnAlmacenes;

/**
 * EmpleadosQueGestionanPedidosEnAlmacenesSearch represents the model behind the search form of `app\models\EmpleadosQueGestionanPedidosEnAlmacenes`.
 */
class EmpleadosQueGestionanPedidosEnAlmacenesSearch extends EmpleadosQueGestionanPedidosEnAlmacenes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_gestion', 'codigo_empleado', 'codigo_pedido', 'codigo_almacen', 'inventario', 'contabilidad'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpleadosQueGestionanPedidosEnAlmacenes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_gestion' => $this->codigo_gestion,
            'codigo_empleado' => $this->codigo_empleado,
            'codigo_pedido' => $this->codigo_pedido,
            'codigo_almacen' => $this->codigo_almacen,
            'inventario' => $this->inventario,
            'contabilidad' => $this->contabilidad,
        ]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_que_componen_ordenes".
 *
 * @property int $codigo_componente
 * @property int|null $codigo_producto
 * @property int|null $codigo_orden
 *
 * @property Productos $codigoProducto
 * @property OrdenesDeFabricacion $codigoOrden
 */
class ProductosQueComponenOrdenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_que_componen_ordenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_producto', 'codigo_orden'], 'integer'],
            [['codigo_producto', 'codigo_orden'], 'unique', 'targetAttribute' => ['codigo_producto', 'codigo_orden']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
            [['codigo_orden'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden' => 'codigo_orden_de_fabricacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_componente' => 'Codigo Componente',
            'codigo_producto' => 'Codigo Producto',
            'codigo_orden' => 'Codigo Orden',
        ];
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoOrden]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrden()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden']);
    }
}

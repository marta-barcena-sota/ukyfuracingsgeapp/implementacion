<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $codigo_empleado
 * @property string|null $nombre_empleado
 * @property string|null $primer_apellido_empleado
 * @property string|null $segundo_apellido_empleado
 * @property string|null $puesto_empleado
 *
 * @property Comentarios[] $comentarios
 * @property Compras[] $compras
 * @property Productos[] $codigoProductos
 * @property EmailsEmpleados[] $emailsEmpleados
 * @property EmpleadosQueGestionanPedidosEnAlmacenes[] $empleadosQueGestionanPedidosEnAlmacenes
 * @property Pedidos[] $codigoPedidos
 * @property EmpleadosQueTrabajanEnProyectos[] $empleadosQueTrabajanEnProyectos
 * @property Proyectos[] $codigoProyectos
 * @property Mantenimientos[] $mantenimientos
 * @property Mejoras[] $mejoras
 * @property OrdenesDeFabricacion[] $ordenesDeFabricacions
 * @property Seguimientos[] $seguimientos
 * @property TlfsEmpleados[] $tlfsEmpleados
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_empleado', 'primer_apellido_empleado', 'segundo_apellido_empleado', 'puesto_empleado'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_empleado' => 'Codigo Empleado',
            'nombre_empleado' => 'Nombre Empleado',
            'primer_apellido_empleado' => 'Primer Apellido Empleado',
            'segundo_apellido_empleado' => 'Segundo Apellido Empleado',
            'puesto_empleado' => 'Puesto Empleado',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('compras', ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[EmailsEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmailsEmpleados()
    {
        return $this->hasMany(EmailsEmpleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[EmpleadosQueGestionanPedidosEnAlmacenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadosQueGestionanPedidosEnAlmacenes()
    {
        return $this->hasMany(EmpleadosQueGestionanPedidosEnAlmacenes::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido'])->viaTable('empleados_que_gestionan_pedidos_en_almacenes', ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[EmpleadosQueTrabajanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadosQueTrabajanEnProyectos()
    {
        return $this->hasMany(EmpleadosQueTrabajanEnProyectos::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto'])->viaTable('empleados_que_trabajan_en_proyectos', ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[Mantenimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientos()
    {
        return $this->hasMany(Mantenimientos::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[Mejoras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMejoras()
    {
        return $this->hasMany(Mejoras::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[OrdenesDeFabricacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenesDeFabricacions()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[Seguimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeguimientos()
    {
        return $this->hasMany(Seguimientos::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[TlfsEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTlfsEmpleados()
    {
        return $this->hasMany(TlfsEmpleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }
}

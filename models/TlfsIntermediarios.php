<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tlfs_intermediarios".
 *
 * @property int $codigo_tlf
 * @property int|null $codigo_intermediario
 * @property string|null $tlf_intermediario
 *
 * @property Intermediarios $codigoIntermediario
 */
class TlfsIntermediarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tlfs_intermediarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_intermediario'], 'integer'],
            [['tlf_intermediario'], 'string', 'max' => 100],
            [['codigo_intermediario', 'tlf_intermediario'], 'unique', 'targetAttribute' => ['codigo_intermediario', 'tlf_intermediario']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tlf' => 'Codigo Tlf',
            'codigo_intermediario' => 'Codigo Intermediario',
            'tlf_intermediario' => 'Tlf Intermediario',
        ];
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductosQueComponenOrdenes;

/**
 * ProductosQueComponenOrdenesSearch represents the model behind the search form of `app\models\ProductosQueComponenOrdenes`.
 */
class ProductosQueComponenOrdenesSearch extends ProductosQueComponenOrdenes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_componente', 'codigo_producto', 'codigo_orden'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductosQueComponenOrdenes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_componente' => $this->codigo_componente,
            'codigo_producto' => $this->codigo_producto,
            'codigo_orden' => $this->codigo_orden,
        ]);

        return $dataProvider;
    }
}

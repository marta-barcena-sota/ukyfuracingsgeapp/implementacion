<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Empleados;

/**
 * EmpleadosSearch represents the model behind the search form of `app\models\Empleados`.
 */
class EmpleadosSearch extends Empleados
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado'], 'integer'],
            [['nombre_empleado', 'primer_apellido_empleado', 'segundo_apellido_empleado', 'puesto_empleado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empleados::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_empleado' => $this->codigo_empleado,
        ]);

        $query->andFilterWhere(['like', 'nombre_empleado', $this->nombre_empleado])
            ->andFilterWhere(['like', 'primer_apellido_empleado', $this->primer_apellido_empleado])
            ->andFilterWhere(['like', 'segundo_apellido_empleado', $this->segundo_apellido_empleado])
            ->andFilterWhere(['like', 'puesto_empleado', $this->puesto_empleado]);

        return $dataProvider;
    }
}

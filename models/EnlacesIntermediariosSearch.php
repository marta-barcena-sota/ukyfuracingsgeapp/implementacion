<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EnlacesIntermediarios;

/**
 * EnlacesIntermediariosSearch represents the model behind the search form of `app\models\EnlacesIntermediarios`.
 */
class EnlacesIntermediariosSearch extends EnlacesIntermediarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_enlace', 'codigo_intermediario'], 'integer'],
            [['enlace_intermediario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EnlacesIntermediarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_enlace' => $this->codigo_enlace,
            'codigo_intermediario' => $this->codigo_intermediario,
        ]);

        $query->andFilterWhere(['like', 'enlace_intermediario', $this->enlace_intermediario]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProveedoresMaterialesOrdenes;

/**
 * ProveedoresMaterialesOrdenesSearch represents the model behind the search form of `app\models\ProveedoresMaterialesOrdenes`.
 */
class ProveedoresMaterialesOrdenesSearch extends ProveedoresMaterialesOrdenes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_suministro', 'codigo_proveedor', 'codigo_material', 'codigo_orden', 'cantidad'], 'integer'],
            [['medida'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProveedoresMaterialesOrdenes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_suministro' => $this->codigo_suministro,
            'codigo_proveedor' => $this->codigo_proveedor,
            'codigo_material' => $this->codigo_material,
            'codigo_orden' => $this->codigo_orden,
            'cantidad' => $this->cantidad,
        ]);

        $query->andFilterWhere(['like', 'medida', $this->medida]);

        return $dataProvider;
    }
}

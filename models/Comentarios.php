<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo_comentario
 * @property int|null $codigo_empleado
 * @property int|null $codigo_seguimiento
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_pedido
 * @property int|null $codigo_producto
 * @property int|null $codigo_cliente
 * @property int|null $codigo_cambio
 * @property int|null $codigo_intermediario
 * @property string|null $concepto_comentario
 * @property string|null $fecha_comentario
 * @property string|null $hora_comentario
 *
 * @property Empleados $codigoEmpleado
 * @property Seguimientos $codigoSeguimiento
 * @property Proveedores $codigoProveedor
 * @property Productos $codigoProducto
 * @property Clientes $codigoCliente
 * @property Intermediarios $codigoIntermediario
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_seguimiento', 'codigo_proveedor', 'codigo_pedido', 'codigo_producto', 'codigo_cliente', 'codigo_cambio', 'codigo_intermediario'], 'integer'],
            [['fecha_comentario', 'hora_comentario'], 'safe'],
            [['concepto_comentario'], 'string', 'max' => 100],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_seguimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Seguimientos::className(), 'targetAttribute' => ['codigo_seguimiento' => 'codigo_seguimiento']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_comentario' => 'Codigo Comentario',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_seguimiento' => 'Codigo Seguimiento',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_producto' => 'Codigo Producto',
            'codigo_cliente' => 'Codigo Cliente',
            'codigo_cambio' => 'Codigo Cambio',
            'codigo_intermediario' => 'Codigo Intermediario',
            'concepto_comentario' => 'Concepto Comentario',
            'fecha_comentario' => 'Fecha Comentario',
            'hora_comentario' => 'Hora Comentario',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoSeguimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoSeguimiento()
    {
        return $this->hasOne(Seguimientos::className(), ['codigo_seguimiento' => 'codigo_seguimiento']);
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}

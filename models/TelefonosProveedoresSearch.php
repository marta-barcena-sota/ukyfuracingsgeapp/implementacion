<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TelefonosProveedores;

/**
 * TelefonosProveedoresSearch represents the model behind the search form of `app\models\TelefonosProveedores`.
 */
class TelefonosProveedoresSearch extends TelefonosProveedores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_tlf_proveedor', 'codigo_proveedor'], 'integer'],
            [['tlf_proveedor'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TelefonosProveedores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_tlf_proveedor' => $this->codigo_tlf_proveedor,
            'codigo_proveedor' => $this->codigo_proveedor,
        ]);

        $query->andFilterWhere(['like', 'tlf_proveedor', $this->tlf_proveedor]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Maquinas;

/**
 * MaquinasSearch represents the model behind the search form of `app\models\Maquinas`.
 */
class MaquinasSearch extends Maquinas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_maquina'], 'integer'],
            [['nombre_maquina', 'numero_de_serie_maquina', 'software_maquina', 'manual_mantenimiento_maquina', 'modelo_maquina', 'manual_procedimiento_maquina', 'carpeta_maquina', 'tipo_maquina', 'descripcion_maquina', 'manual_maquina', 'firmware_maquina', 'fabricante_maquina'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maquinas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_maquina' => $this->codigo_maquina,
        ]);

        $query->andFilterWhere(['like', 'nombre_maquina', $this->nombre_maquina])
            ->andFilterWhere(['like', 'numero_de_serie_maquina', $this->numero_de_serie_maquina])
            ->andFilterWhere(['like', 'software_maquina', $this->software_maquina])
            ->andFilterWhere(['like', 'manual_mantenimiento_maquina', $this->manual_mantenimiento_maquina])
            ->andFilterWhere(['like', 'modelo_maquina', $this->modelo_maquina])
            ->andFilterWhere(['like', 'manual_procedimiento_maquina', $this->manual_procedimiento_maquina])
            ->andFilterWhere(['like', 'carpeta_maquina', $this->carpeta_maquina])
            ->andFilterWhere(['like', 'tipo_maquina', $this->tipo_maquina])
            ->andFilterWhere(['like', 'descripcion_maquina', $this->descripcion_maquina])
            ->andFilterWhere(['like', 'manual_maquina', $this->manual_maquina])
            ->andFilterWhere(['like', 'firmware_maquina', $this->firmware_maquina])
            ->andFilterWhere(['like', 'fabricante_maquina', $this->fabricante_maquina]);

        return $dataProvider;
    }
}

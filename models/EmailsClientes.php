<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emails_clientes".
 *
 * @property int $codigo_email
 * @property int|null $codigo_cliente
 * @property string|null $email_cliente
 *
 * @property Clientes $codigoCliente
 */
class EmailsClientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails_clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['email_cliente'], 'string', 'max' => 100],
            [['codigo_cliente', 'email_cliente'], 'unique', 'targetAttribute' => ['codigo_cliente', 'email_cliente']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_email' => 'Codigo Email',
            'codigo_cliente' => 'Codigo Cliente',
            'email_cliente' => 'Email Cliente',
        ];
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_componen_proyectos".
 *
 * @property int $codigo_componente
 * @property int|null $codigo_proyecto
 * @property int|null $codigo_producto
 *
 * @property Proyectos $codigoProyecto
 * @property Productos $codigoProducto
 */
class ProductosComponenProyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_componen_proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proyecto', 'codigo_producto'], 'integer'],
            [['codigo_proyecto', 'codigo_producto'], 'unique', 'targetAttribute' => ['codigo_proyecto', 'codigo_producto']],
            [['codigo_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['codigo_proyecto' => 'codigo_proyecto']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_componente' => 'Codigo Componente',
            'codigo_proyecto' => 'Codigo Proyecto',
            'codigo_producto' => 'Codigo Producto',
        ];
    }

    /**
     * Gets query for [[CodigoProyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_participan_en_proyectos".
 *
 * @property int $codigo_participacion
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_proyecto
 *
 * @property Proveedores $codigoProveedor
 * @property Proyectos $codigoProyecto
 */
class ProveedoresQueParticipanEnProyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_participan_en_proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_proyecto'], 'integer'],
            [['codigo_proveedor', 'codigo_proyecto'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_proyecto']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['codigo_proyecto' => 'codigo_proyecto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_participacion' => 'Codigo Participacion',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_proyecto' => 'Codigo Proyecto',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoProyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }
}

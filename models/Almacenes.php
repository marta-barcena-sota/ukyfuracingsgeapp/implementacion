<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "almacenes".
 *
 * @property int $codigo_almacen
 * @property string|null $denominacion_almacen
 * @property string|null $direccion_almacen
 *
 * @property AlmacenesQueGestionanCambios[] $almacenesQueGestionanCambios
 * @property Cambios[] $codigoCambios
 * @property AlmacenesQueGuardanProductos[] $almacenesQueGuardanProductos
 * @property Productos[] $codigoProductos
 * @property EmpleadosQueGestionanPedidosEnAlmacenes[] $empleadosQueGestionanPedidosEnAlmacenes
 * @property Pedidos[] $codigoPedidos
 */
class Almacenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'almacenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['denominacion_almacen', 'direccion_almacen'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_almacen' => 'Codigo Almacen',
            'denominacion_almacen' => 'Denominacion Almacen',
            'direccion_almacen' => 'Direccion Almacen',
        ];
    }

    /**
     * Gets query for [[AlmacenesQueGestionanCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacenesQueGestionanCambios()
    {
        return $this->hasMany(AlmacenesQueGestionanCambios::className(), ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[CodigoCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCambios()
    {
        return $this->hasMany(Cambios::className(), ['codigo_cambio' => 'codigo_cambio'])->viaTable('almacenes_que_gestionan_cambios', ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[AlmacenesQueGuardanProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacenesQueGuardanProductos()
    {
        return $this->hasMany(AlmacenesQueGuardanProductos::className(), ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('almacenes_que_guardan_productos', ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[EmpleadosQueGestionanPedidosEnAlmacenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadosQueGestionanPedidosEnAlmacenes()
    {
        return $this->hasMany(EmpleadosQueGestionanPedidosEnAlmacenes::className(), ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[CodigoPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido'])->viaTable('empleados_que_gestionan_pedidos_en_almacenes', ['codigo_almacen' => 'codigo_almacen']);
    }
}

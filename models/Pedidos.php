<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidos".
 *
 * @property int $codigo_pedido
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_cliente
 * @property string|null $tracking_pedido
 * @property string|null $carpeta_pedido
 * @property string|null $tipo_pedido
 * @property string|null $estado_pedido
 * @property string|null $urgencia_pedido
 * @property string|null $forma_de_pago
 * @property int|null $hay_pago_importacion
 * @property float|null $pago_importacion
 * @property string|null $fecha_suministro
 * @property string|null $fecha_encargo
 *
 * @property Documentaciones[] $documentaciones
 * @property EmpleadosQueGestionanPedidosEnAlmacenes[] $empleadosQueGestionanPedidosEnAlmacenes
 * @property Empleados[] $codigoEmpleados
 * @property Almacenes[] $codigoAlmacens
 * @property Proveedores $codigoProveedor
 * @property Clientes $codigoCliente
 * @property ProductosQueComponenPedidos[] $productosQueComponenPedidos
 * @property Productos[] $codigoProductos
 * @property Seguimientos[] $seguimientos
 * @property TransportesQueSufrenLosPedidos[] $transportesQueSufrenLosPedidos
 * @property Transportes[] $codigoTransportes
 */
class Pedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_cliente', 'hay_pago_importacion'], 'integer'],
            [['pago_importacion'], 'number'],
            [['fecha_suministro', 'fecha_encargo'], 'safe'],
            [['tracking_pedido', 'carpeta_pedido', 'tipo_pedido', 'estado_pedido', 'urgencia_pedido', 'forma_de_pago'], 'string', 'max' => 100],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_cliente' => 'Codigo Cliente',
            'tracking_pedido' => 'Tracking Pedido',
            'carpeta_pedido' => 'Carpeta Pedido',
            'tipo_pedido' => 'Tipo Pedido',
            'estado_pedido' => 'Estado Pedido',
            'urgencia_pedido' => 'Urgencia Pedido',
            'forma_de_pago' => 'Forma De Pago',
            'hay_pago_importacion' => 'Hay Pago Importacion',
            'pago_importacion' => 'Pago Importacion',
            'fecha_suministro' => 'Fecha Suministro',
            'fecha_encargo' => 'Fecha Encargo',
        ];
    }

    /**
     * Gets query for [[Documentaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentaciones()
    {
        return $this->hasMany(Documentaciones::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[EmpleadosQueGestionanPedidosEnAlmacenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadosQueGestionanPedidosEnAlmacenes()
    {
        return $this->hasMany(EmpleadosQueGestionanPedidosEnAlmacenes::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_empleado' => 'codigo_empleado'])->viaTable('empleados_que_gestionan_pedidos_en_almacenes', ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoAlmacens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacens()
    {
        return $this->hasMany(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen'])->viaTable('empleados_que_gestionan_pedidos_en_almacenes', ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[ProductosQueComponenPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueComponenPedidos()
    {
        return $this->hasMany(ProductosQueComponenPedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('productos_que_componen_pedidos', ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[Seguimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeguimientos()
    {
        return $this->hasMany(Seguimientos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[TransportesQueSufrenLosPedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransportesQueSufrenLosPedidos()
    {
        return $this->hasMany(TransportesQueSufrenLosPedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoTransportes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoTransportes()
    {
        return $this->hasMany(Transportes::className(), ['codigo_transporte' => 'codigo_transporte'])->viaTable('transportes_que_sufren_los_pedidos', ['codigo_pedido' => 'codigo_pedido']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $codigo_proveedor
 * @property string|null $cif_proveedor
 * @property string|null $eori_proveedor
 * @property string|null $nombre_proveedor
 * @property string|null $tipo_de_empresa_proveedor
 * @property string|null $sociedad_empresa_proveedor
 * @property string|null $pais_proveedor
 * @property string|null $tipo_proveedor
 *
 * @property Comentarios[] $comentarios
 * @property EmailsProveedores[] $emailsProveedores
 * @property Pedidos[] $pedidos
 * @property ProveedoresMaterialesOrdenes[] $proveedoresMaterialesOrdenes
 * @property Materiales[] $codigoMaterials
 * @property ProveedoresQueColaboranEnOrdenes[] $proveedoresQueColaboranEnOrdenes
 * @property OrdenesDeFabricacion[] $codigoOrdenFabricacions
 * @property ProveedoresQueParticipanEnProyectos[] $proveedoresQueParticipanEnProyectos
 * @property Proyectos[] $codigoProyectos
 * @property ProveedoresQueSuministranMateriales[] $proveedoresQueSuministranMateriales
 * @property Materiales[] $codigoMaterials0
 * @property ProveedoresQueSuministranPostprocesados[] $proveedoresQueSuministranPostprocesados
 * @property Postprocesados[] $codigoPostprocesados
 * @property ProveedoresQueSuministranPostprocesadosAOrdenes[] $proveedoresQueSuministranPostprocesadosAOrdenes
 * @property Postprocesados[] $codigoPostprocesados0
 * @property OrdenesDeFabricacion[] $codigoOrdens
 * @property ProveedoresQueSuministranProductos[] $proveedoresQueSuministranProductos
 * @property Productos[] $codigoProductos
 * @property TelefonosProveedores[] $telefonosProveedores
 * @property Transportes[] $transportes
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif_proveedor'], 'string', 'max' => 9],
            [['eori_proveedor'], 'string', 'max' => 50],
            [['nombre_proveedor', 'tipo_de_empresa_proveedor', 'sociedad_empresa_proveedor', 'tipo_proveedor'], 'string', 'max' => 100],
            [['pais_proveedor'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_proveedor' => 'Codigo Proveedor',
            'cif_proveedor' => 'Cif Proveedor',
            'eori_proveedor' => 'Eori Proveedor',
            'nombre_proveedor' => 'Nombre Proveedor',
            'tipo_de_empresa_proveedor' => 'Tipo De Empresa Proveedor',
            'sociedad_empresa_proveedor' => 'Sociedad Empresa Proveedor',
            'pais_proveedor' => 'Pais Proveedor',
            'tipo_proveedor' => 'Tipo Proveedor',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[EmailsProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmailsProveedores()
    {
        return $this->hasMany(EmailsProveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresMaterialesOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresMaterialesOrdenes()
    {
        return $this->hasMany(ProveedoresMaterialesOrdenes::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoMaterials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaterials()
    {
        return $this->hasMany(Materiales::className(), ['codigo_material' => 'codigo_material'])->viaTable('proveedores_materiales_ordenes', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueColaboranEnOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueColaboranEnOrdenes()
    {
        return $this->hasMany(ProveedoresQueColaboranEnOrdenes::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoOrdenFabricacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdenFabricacions()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden_fabricacion'])->viaTable('proveedores_que_colaboran_en_ordenes', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueParticipanEnProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueParticipanEnProyectos()
    {
        return $this->hasMany(ProveedoresQueParticipanEnProyectos::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoProyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto'])->viaTable('proveedores_que_participan_en_proyectos', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranMateriales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranMateriales()
    {
        return $this->hasMany(ProveedoresQueSuministranMateriales::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoMaterials0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaterials0()
    {
        return $this->hasMany(Materiales::className(), ['codigo_material' => 'codigo_material'])->viaTable('proveedores_que_suministran_materiales', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranPostprocesados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranPostprocesados()
    {
        return $this->hasMany(ProveedoresQueSuministranPostprocesados::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoPostprocesados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostprocesados()
    {
        return $this->hasMany(Postprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado'])->viaTable('proveedores_que_suministran_postprocesados', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranPostprocesadosAOrdenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranPostprocesadosAOrdenes()
    {
        return $this->hasMany(ProveedoresQueSuministranPostprocesadosAOrdenes::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoPostprocesados0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostprocesados0()
    {
        return $this->hasMany(Postprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoOrdens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdens()
    {
        return $this->hasMany(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden'])->viaTable('proveedores_que_suministran_postprocesados_a_ordenes', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[ProveedoresQueSuministranProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedoresQueSuministranProductos()
    {
        return $this->hasMany(ProveedoresQueSuministranProductos::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('proveedores_que_suministran_productos', ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[TelefonosProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosProveedores()
    {
        return $this->hasMany(TelefonosProveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[Transportes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransportes()
    {
        return $this->hasMany(Transportes::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emails_proveedores".
 *
 * @property int $codigo_email_proveedor
 * @property int|null $codigo_proveedor
 * @property string|null $email_proveedor
 *
 * @property Proveedores $codigoProveedor
 */
class EmailsProveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails_proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor'], 'integer'],
            [['email_proveedor'], 'string', 'max' => 100],
            [['codigo_proveedor', 'email_proveedor'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'email_proveedor']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_email_proveedor' => 'Codigo Email Proveedor',
            'codigo_proveedor' => 'Codigo Proveedor',
            'email_proveedor' => 'Email Proveedor',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }
}

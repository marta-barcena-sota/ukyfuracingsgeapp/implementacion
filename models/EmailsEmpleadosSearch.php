<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmailsEmpleados;

/**
 * EmailsEmpleadosSearch represents the model behind the search form of `app\models\EmailsEmpleados`.
 */
class EmailsEmpleadosSearch extends EmailsEmpleados
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_email', 'codigo_empleado'], 'integer'],
            [['email_empleado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmailsEmpleados::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_email' => $this->codigo_email,
            'codigo_empleado' => $this->codigo_empleado,
        ]);

        $query->andFilterWhere(['like', 'email_empleado', $this->email_empleado]);

        return $dataProvider;
    }
}

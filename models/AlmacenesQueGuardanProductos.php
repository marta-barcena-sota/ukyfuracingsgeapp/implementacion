<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "almacenes_que_guardan_productos".
 *
 * @property int $codigo_almacenamiento
 * @property int|null $codigo_almacen
 * @property int|null $codigo_producto
 * @property string|null $fecha_ingreso_producto
 * @property int|null $cantidad
 *
 * @property Almacenes $codigoAlmacen
 * @property Productos $codigoProducto
 */
class AlmacenesQueGuardanProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'almacenes_que_guardan_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_almacen', 'codigo_producto', 'cantidad'], 'integer'],
            [['fecha_ingreso_producto'], 'safe'],
            [['codigo_almacen', 'codigo_producto'], 'unique', 'targetAttribute' => ['codigo_almacen', 'codigo_producto']],
            [['codigo_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::className(), 'targetAttribute' => ['codigo_almacen' => 'codigo_almacen']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_almacenamiento' => 'Codigo Almacenamiento',
            'codigo_almacen' => 'Codigo Almacen',
            'codigo_producto' => 'Codigo Producto',
            'fecha_ingreso_producto' => 'Fecha Ingreso Producto',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[CodigoAlmacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacen()
    {
        return $this->hasOne(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }
}

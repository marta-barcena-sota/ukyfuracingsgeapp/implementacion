<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmailsProveedores;

/**
 * EmailsProveedoresSearch represents the model behind the search form of `app\models\EmailsProveedores`.
 */
class EmailsProveedoresSearch extends EmailsProveedores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_email_proveedor', 'codigo_proveedor'], 'integer'],
            [['email_proveedor'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmailsProveedores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_email_proveedor' => $this->codigo_email_proveedor,
            'codigo_proveedor' => $this->codigo_proveedor,
        ]);

        $query->andFilterWhere(['like', 'email_proveedor', $this->email_proveedor]);

        return $dataProvider;
    }
}

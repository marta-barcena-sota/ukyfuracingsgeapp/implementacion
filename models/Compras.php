<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $codigo_compra
 * @property int|null $codigo_empleado
 * @property int|null $codigo_producto
 * @property int|null $cantidad_adquirida
 * @property string|null $fecha_adquisicion
 *
 * @property Empleados $codigoEmpleado
 * @property Productos $codigoProducto
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_producto', 'cantidad_adquirida'], 'integer'],
            [['fecha_adquisicion'], 'safe'],
            [['codigo_empleado', 'codigo_producto'], 'unique', 'targetAttribute' => ['codigo_empleado', 'codigo_producto']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['codigo_producto' => 'codigo_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_compra' => 'Codigo Compra',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_producto' => 'Codigo Producto',
            'cantidad_adquirida' => 'Cantidad Adquirida',
            'fecha_adquisicion' => 'Fecha Adquisicion',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }
}

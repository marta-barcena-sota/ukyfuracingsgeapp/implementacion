<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $codigo_cliente
 * @property string|null $pais_cliente
 * @property string|null $cif_cliente
 * @property string|null $nombre_cliente
 * @property string|null $tipo_empresa_cliente
 * @property string|null $sociedad_empresa_cliente
 * @property string|null $nombre_contacto_cliente
 * @property string|null $primer_apellido_contacto_cliente
 * @property string|null $segundo_apellido_contacto_cliente
 *
 * @property Comentarios[] $comentarios
 * @property DireccionesEnvioClientes[] $direccionesEnvioClientes
 * @property DireccionesFacturacionClientes[] $direccionesFacturacionClientes
 * @property EmailsClientes[] $emailsClientes
 * @property Pedidos[] $pedidos
 * @property Proyectos[] $proyectos
 * @property TlfsClientes[] $tlfsClientes
 * @property Ventas[] $ventas
 * @property Productos[] $codProductos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pais_cliente'], 'string', 'max' => 2],
            [['cif_cliente'], 'string', 'max' => 9],
            [['nombre_cliente', 'tipo_empresa_cliente', 'sociedad_empresa_cliente', 'nombre_contacto_cliente', 'primer_apellido_contacto_cliente', 'segundo_apellido_contacto_cliente'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_cliente' => 'Codigo Cliente',
            'pais_cliente' => 'Pais Cliente',
            'cif_cliente' => 'Cif Cliente',
            'nombre_cliente' => 'Nombre Cliente',
            'tipo_empresa_cliente' => 'Tipo Empresa Cliente',
            'sociedad_empresa_cliente' => 'Sociedad Empresa Cliente',
            'nombre_contacto_cliente' => 'Nombre Contacto Cliente',
            'primer_apellido_contacto_cliente' => 'Primer Apellido Contacto Cliente',
            'segundo_apellido_contacto_cliente' => 'Segundo Apellido Contacto Cliente',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[DireccionesEnvioClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDireccionesEnvioClientes()
    {
        return $this->hasMany(DireccionesEnvioClientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[DireccionesFacturacionClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDireccionesFacturacionClientes()
    {
        return $this->hasMany(DireccionesFacturacionClientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[EmailsClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmailsClientes()
    {
        return $this->hasMany(EmailsClientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[Proyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[TlfsClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTlfsClientes()
    {
        return $this->hasMany(TlfsClientes::className(), ['codigo_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['cod_cliente' => 'codigo_cliente']);
    }

    /**
     * Gets query for [[CodProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'cod_producto'])->viaTable('ventas', ['cod_cliente' => 'codigo_cliente']);
    }
}

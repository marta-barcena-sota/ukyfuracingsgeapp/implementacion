<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TlfsIntermediarios;

/**
 * TlfsIntermediariosSearch represents the model behind the search form of `app\models\TlfsIntermediarios`.
 */
class TlfsIntermediariosSearch extends TlfsIntermediarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_tlf', 'codigo_intermediario'], 'integer'],
            [['tlf_intermediario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TlfsIntermediarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_tlf' => $this->codigo_tlf,
            'codigo_intermediario' => $this->codigo_intermediario,
        ]);

        $query->andFilterWhere(['like', 'tlf_intermediario', $this->tlf_intermediario]);

        return $dataProvider;
    }
}

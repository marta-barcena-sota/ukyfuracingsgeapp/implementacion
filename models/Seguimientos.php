<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seguimientos".
 *
 * @property int $codigo_seguimiento
 * @property int|null $codigo_pedido
 * @property int|null $codigo_orden_de_fabricacion
 * @property int|null $codigo_empleado
 * @property string|null $hora_seguimiento
 * @property string|null $fecha_de_realizacion_seguimiento
 *
 * @property Comentarios[] $comentarios
 * @property EnlacesSeguimientos[] $enlacesSeguimientos
 * @property Pedidos $codigoPedido
 * @property OrdenesDeFabricacion $codigoOrdenDeFabricacion
 * @property Empleados $codigoEmpleado
 */
class Seguimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seguimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_pedido', 'codigo_orden_de_fabricacion', 'codigo_empleado'], 'integer'],
            [['hora_seguimiento', 'fecha_de_realizacion_seguimiento'], 'safe'],
            [['codigo_pedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidos::className(), 'targetAttribute' => ['codigo_pedido' => 'codigo_pedido']],
            [['codigo_orden_de_fabricacion'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden_de_fabricacion' => 'codigo_orden_de_fabricacion']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_seguimiento' => 'Codigo Seguimiento',
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_orden_de_fabricacion' => 'Codigo Orden De Fabricacion',
            'codigo_empleado' => 'Codigo Empleado',
            'hora_seguimiento' => 'Hora Seguimiento',
            'fecha_de_realizacion_seguimiento' => 'Fecha De Realizacion Seguimiento',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['codigo_seguimiento' => 'codigo_seguimiento']);
    }

    /**
     * Gets query for [[EnlacesSeguimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEnlacesSeguimientos()
    {
        return $this->hasMany(EnlacesSeguimientos::className(), ['codigo_seguimiento' => 'codigo_seguimiento']);
    }

    /**
     * Gets query for [[CodigoPedido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedido()
    {
        return $this->hasOne(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoOrdenDeFabricacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrdenDeFabricacion()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden_de_fabricacion']);
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_suministran_postprocesados".
 *
 * @property int $codigo_suministro
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_postprocesado
 *
 * @property Proveedores $codigoProveedor
 * @property Postprocesados $codigoPostprocesado
 */
class ProveedoresQueSuministranPostprocesados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_suministran_postprocesados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_postprocesado'], 'integer'],
            [['codigo_proveedor', 'codigo_postprocesado'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_postprocesado']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_postprocesado'], 'exist', 'skipOnError' => true, 'targetClass' => Postprocesados::className(), 'targetAttribute' => ['codigo_postprocesado' => 'codigo_postprocesado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suministro' => 'Codigo Suministro',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_postprocesado' => 'Codigo Postprocesado',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoPostprocesado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostprocesado()
    {
        return $this->hasOne(Postprocesados::className(), ['codigo_postprocesado' => 'codigo_postprocesado']);
    }
}

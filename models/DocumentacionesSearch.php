<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Documentaciones;

/**
 * DocumentacionesSearch represents the model behind the search form of `app\models\Documentaciones`.
 */
class DocumentacionesSearch extends Documentaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_documentacion', 'codigo_pedido', 'codigo_producto', 'numero_de_factura'], 'integer'],
            [['documento_de_transporte', 'justificante_de_pago', 'fecha_factura', 'archivo_factura', 'albaran', 'pedido', 'documento_importacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documentaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_documentacion' => $this->codigo_documentacion,
            'codigo_pedido' => $this->codigo_pedido,
            'codigo_producto' => $this->codigo_producto,
            'numero_de_factura' => $this->numero_de_factura,
            'fecha_factura' => $this->fecha_factura,
        ]);

        $query->andFilterWhere(['like', 'documento_de_transporte', $this->documento_de_transporte])
            ->andFilterWhere(['like', 'justificante_de_pago', $this->justificante_de_pago])
            ->andFilterWhere(['like', 'archivo_factura', $this->archivo_factura])
            ->andFilterWhere(['like', 'albaran', $this->albaran])
            ->andFilterWhere(['like', 'pedido', $this->pedido])
            ->andFilterWhere(['like', 'documento_importacion', $this->documento_importacion]);

        return $dataProvider;
    }
}

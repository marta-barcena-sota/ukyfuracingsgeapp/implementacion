<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clientes;

/**
 * ClientesSearch represents the model behind the search form of `app\models\Clientes`.
 */
class ClientesSearch extends Clientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['pais_cliente', 'cif_cliente', 'nombre_cliente', 'tipo_empresa_cliente', 'sociedad_empresa_cliente', 'nombre_contacto_cliente', 'primer_apellido_contacto_cliente', 'segundo_apellido_contacto_cliente'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_cliente' => $this->codigo_cliente,
        ]);

        $query->andFilterWhere(['like', 'pais_cliente', $this->pais_cliente])
            ->andFilterWhere(['like', 'cif_cliente', $this->cif_cliente])
            ->andFilterWhere(['like', 'nombre_cliente', $this->nombre_cliente])
            ->andFilterWhere(['like', 'tipo_empresa_cliente', $this->tipo_empresa_cliente])
            ->andFilterWhere(['like', 'sociedad_empresa_cliente', $this->sociedad_empresa_cliente])
            ->andFilterWhere(['like', 'nombre_contacto_cliente', $this->nombre_contacto_cliente])
            ->andFilterWhere(['like', 'primer_apellido_contacto_cliente', $this->primer_apellido_contacto_cliente])
            ->andFilterWhere(['like', 'segundo_apellido_contacto_cliente', $this->segundo_apellido_contacto_cliente]);

        return $dataProvider;
    }
}

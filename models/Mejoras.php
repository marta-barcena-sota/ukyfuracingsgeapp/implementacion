<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mejoras".
 *
 * @property int $codigo_mejora
 * @property int|null $codigo_empleado
 * @property string|null $descripcion_mejora
 * @property string|null $carpeta_mejora
 * @property string|null $fecha
 *
 * @property Empleados $codigoEmpleado
 * @property MejorasDeMaquinas[] $mejorasDeMaquinas
 * @property Maquinas[] $codigoMaquinas
 */
class Mejoras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mejoras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado'], 'integer'],
            [['fecha'], 'safe'],
            [['descripcion_mejora', 'carpeta_mejora'], 'string', 'max' => 100],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_mejora' => 'Codigo Mejora',
            'codigo_empleado' => 'Codigo Empleado',
            'descripcion_mejora' => 'Descripcion Mejora',
            'carpeta_mejora' => 'Carpeta Mejora',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[MejorasDeMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMejorasDeMaquinas()
    {
        return $this->hasMany(MejorasDeMaquinas::className(), ['codigo_mejora' => 'codigo_mejora']);
    }

    /**
     * Gets query for [[CodigoMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquinas()
    {
        return $this->hasMany(Maquinas::className(), ['codigo_maquina' => 'codigo_maquina'])->viaTable('mejoras_de_maquinas', ['codigo_mejora' => 'codigo_mejora']);
    }
}

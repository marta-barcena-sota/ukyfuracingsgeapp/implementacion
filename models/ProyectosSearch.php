<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proyectos;

/**
 * ProyectosSearch represents the model behind the search form of `app\models\Proyectos`.
 */
class ProyectosSearch extends Proyectos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proyecto', 'codigo_cliente'], 'integer'],
            [['referencia_interna_proyecto', 'nombre_proyecto', 'carpeta_proyecto', 'referencia_cad_proyecto', 'gantt_proyecto', 'fecha_fin_proyecto', 'fecha_inicio_proyecto', 'tipo_proyecto', 'estado_proyecto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proyectos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_proyecto' => $this->codigo_proyecto,
            'codigo_cliente' => $this->codigo_cliente,
            'fecha_fin_proyecto' => $this->fecha_fin_proyecto,
            'fecha_inicio_proyecto' => $this->fecha_inicio_proyecto,
        ]);

        $query->andFilterWhere(['like', 'referencia_interna_proyecto', $this->referencia_interna_proyecto])
            ->andFilterWhere(['like', 'nombre_proyecto', $this->nombre_proyecto])
            ->andFilterWhere(['like', 'carpeta_proyecto', $this->carpeta_proyecto])
            ->andFilterWhere(['like', 'referencia_cad_proyecto', $this->referencia_cad_proyecto])
            ->andFilterWhere(['like', 'gantt_proyecto', $this->gantt_proyecto])
            ->andFilterWhere(['like', 'tipo_proyecto', $this->tipo_proyecto])
            ->andFilterWhere(['like', 'estado_proyecto', $this->estado_proyecto]);

        return $dataProvider;
    }
}

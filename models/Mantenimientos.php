<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mantenimientos".
 *
 * @property int $codigo_mantenimiento
 * @property int|null $codigo_empleado
 * @property int|null $codigo_maquina
 * @property string|null $version_firmware
 * @property string|null $fecha_de_realizacion_mantenimiento
 *
 * @property Empleados $codigoEmpleado
 * @property Maquinas $codigoMaquina
 */
class Mantenimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mantenimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_maquina'], 'integer'],
            [['fecha_de_realizacion_mantenimiento'], 'safe'],
            [['version_firmware'], 'string', 'max' => 100],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_maquina'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::className(), 'targetAttribute' => ['codigo_maquina' => 'codigo_maquina']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_mantenimiento' => 'Codigo Mantenimiento',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_maquina' => 'Codigo Maquina',
            'version_firmware' => 'Version Firmware',
            'fecha_de_realizacion_mantenimiento' => 'Fecha De Realizacion Mantenimiento',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoMaquina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquina()
    {
        return $this->hasOne(Maquinas::className(), ['codigo_maquina' => 'codigo_maquina']);
    }
}

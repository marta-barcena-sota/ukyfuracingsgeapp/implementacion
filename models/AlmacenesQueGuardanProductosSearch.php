<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AlmacenesQueGuardanProductos;

/**
 * AlmacenesQueGuardanProductosSearch represents the model behind the search form of `app\models\AlmacenesQueGuardanProductos`.
 */
class AlmacenesQueGuardanProductosSearch extends AlmacenesQueGuardanProductos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_almacenamiento', 'codigo_almacen', 'codigo_producto', 'cantidad'], 'integer'],
            [['fecha_ingreso_producto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlmacenesQueGuardanProductos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_almacenamiento' => $this->codigo_almacenamiento,
            'codigo_almacen' => $this->codigo_almacen,
            'codigo_producto' => $this->codigo_producto,
            'fecha_ingreso_producto' => $this->fecha_ingreso_producto,
            'cantidad' => $this->cantidad,
        ]);

        return $dataProvider;
    }
}

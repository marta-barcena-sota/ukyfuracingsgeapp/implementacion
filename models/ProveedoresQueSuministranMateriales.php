<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores_que_suministran_materiales".
 *
 * @property int $codigo_suministro
 * @property int|null $codigo_proveedor
 * @property int|null $codigo_material
 *
 * @property Proveedores $codigoProveedor
 * @property Materiales $codigoMaterial
 */
class ProveedoresQueSuministranMateriales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores_que_suministran_materiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proveedor', 'codigo_material'], 'integer'],
            [['codigo_proveedor', 'codigo_material'], 'unique', 'targetAttribute' => ['codigo_proveedor', 'codigo_material']],
            [['codigo_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['codigo_proveedor' => 'codigo_proveedor']],
            [['codigo_material'], 'exist', 'skipOnError' => true, 'targetClass' => Materiales::className(), 'targetAttribute' => ['codigo_material' => 'codigo_material']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suministro' => 'Codigo Suministro',
            'codigo_proveedor' => 'Codigo Proveedor',
            'codigo_material' => 'Codigo Material',
        ];
    }

    /**
     * Gets query for [[CodigoProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['codigo_proveedor' => 'codigo_proveedor']);
    }

    /**
     * Gets query for [[CodigoMaterial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaterial()
    {
        return $this->hasOne(Materiales::className(), ['codigo_material' => 'codigo_material']);
    }
}

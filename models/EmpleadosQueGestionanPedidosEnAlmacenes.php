<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados_que_gestionan_pedidos_en_almacenes".
 *
 * @property int $codigo_gestion
 * @property int|null $codigo_empleado
 * @property int|null $codigo_pedido
 * @property int|null $codigo_almacen
 * @property int|null $inventario
 * @property int|null $contabilidad
 *
 * @property Empleados $codigoEmpleado
 * @property Pedidos $codigoPedido
 * @property Almacenes $codigoAlmacen
 */
class EmpleadosQueGestionanPedidosEnAlmacenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados_que_gestionan_pedidos_en_almacenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_pedido', 'codigo_almacen', 'inventario', 'contabilidad'], 'integer'],
            [['codigo_empleado', 'codigo_pedido'], 'unique', 'targetAttribute' => ['codigo_empleado', 'codigo_pedido']],
            [['codigo_pedido', 'codigo_almacen'], 'unique', 'targetAttribute' => ['codigo_pedido', 'codigo_almacen']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_pedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidos::className(), 'targetAttribute' => ['codigo_pedido' => 'codigo_pedido']],
            [['codigo_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::className(), 'targetAttribute' => ['codigo_almacen' => 'codigo_almacen']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_gestion' => 'Codigo Gestion',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_almacen' => 'Codigo Almacen',
            'inventario' => 'Inventario',
            'contabilidad' => 'Contabilidad',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }

    /**
     * Gets query for [[CodigoPedido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedido()
    {
        return $this->hasOne(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoAlmacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacen()
    {
        return $this->hasOne(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen']);
    }
}

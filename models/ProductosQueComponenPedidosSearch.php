<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductosQueComponenPedidos;

/**
 * ProductosQueComponenPedidosSearch represents the model behind the search form of `app\models\ProductosQueComponenPedidos`.
 */
class ProductosQueComponenPedidosSearch extends ProductosQueComponenPedidos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_componente', 'codigo_producto', 'codigo_pedido', 'cantidad', 'cantidad_pendiente', 'realizado'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductosQueComponenPedidos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_componente' => $this->codigo_componente,
            'codigo_producto' => $this->codigo_producto,
            'codigo_pedido' => $this->codigo_pedido,
            'cantidad' => $this->cantidad,
            'cantidad_pendiente' => $this->cantidad_pendiente,
            'realizado' => $this->realizado,
        ]);

        return $dataProvider;
    }
}

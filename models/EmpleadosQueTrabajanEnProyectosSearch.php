<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpleadosQueTrabajanEnProyectos;

/**
 * EmpleadosQueTrabajanEnProyectosSearch represents the model behind the search form of `app\models\EmpleadosQueTrabajanEnProyectos`.
 */
class EmpleadosQueTrabajanEnProyectosSearch extends EmpleadosQueTrabajanEnProyectos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_trabajo', 'codigo_proyecto', 'codigo_empleado', 'responsable'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpleadosQueTrabajanEnProyectos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_trabajo' => $this->codigo_trabajo,
            'codigo_proyecto' => $this->codigo_proyecto,
            'codigo_empleado' => $this->codigo_empleado,
            'responsable' => $this->responsable,
        ]);

        return $dataProvider;
    }
}

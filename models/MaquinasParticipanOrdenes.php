<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas_participan_ordenes".
 *
 * @property int $codigo_operacion
 * @property int|null $codigo_maquina
 * @property int|null $codigo_orden
 *
 * @property Maquinas $codigoMaquina
 * @property OrdenesDeFabricacion $codigoOrden
 */
class MaquinasParticipanOrdenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquinas_participan_ordenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_maquina', 'codigo_orden'], 'integer'],
            [['codigo_maquina', 'codigo_orden'], 'unique', 'targetAttribute' => ['codigo_maquina', 'codigo_orden']],
            [['codigo_maquina'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::className(), 'targetAttribute' => ['codigo_maquina' => 'codigo_maquina']],
            [['codigo_orden'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenesDeFabricacion::className(), 'targetAttribute' => ['codigo_orden' => 'codigo_orden_de_fabricacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_operacion' => 'Codigo Operacion',
            'codigo_maquina' => 'Codigo Maquina',
            'codigo_orden' => 'Codigo Orden',
        ];
    }

    /**
     * Gets query for [[CodigoMaquina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquina()
    {
        return $this->hasOne(Maquinas::className(), ['codigo_maquina' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[CodigoOrden]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOrden()
    {
        return $this->hasOne(OrdenesDeFabricacion::className(), ['codigo_orden_de_fabricacion' => 'codigo_orden']);
    }
}

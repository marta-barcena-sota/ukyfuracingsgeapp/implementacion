<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Seguimientos;

/**
 * SeguimientosSearch represents the model behind the search form of `app\models\Seguimientos`.
 */
class SeguimientosSearch extends Seguimientos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_seguimiento', 'codigo_pedido', 'codigo_orden_de_fabricacion', 'codigo_empleado'], 'integer'],
            [['hora_seguimiento', 'fecha_de_realizacion_seguimiento'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Seguimientos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_seguimiento' => $this->codigo_seguimiento,
            'codigo_pedido' => $this->codigo_pedido,
            'codigo_orden_de_fabricacion' => $this->codigo_orden_de_fabricacion,
            'codigo_empleado' => $this->codigo_empleado,
            'hora_seguimiento' => $this->hora_seguimiento,
            'fecha_de_realizacion_seguimiento' => $this->fecha_de_realizacion_seguimiento,
        ]);

        return $dataProvider;
    }
}

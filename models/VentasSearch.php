<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ventas;

/**
 * VentasSearch represents the model behind the search form of `app\models\Ventas`.
 */
class VentasSearch extends Ventas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_venta', 'cod_cliente', 'cod_producto', 'cantidad'], 'integer'],
            [['base_imponible_unitaria_venta', 'beneficio'], 'number'],
            [['fecha_de_salida', 'referencia_interna_venta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ventas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_venta' => $this->codigo_venta,
            'cod_cliente' => $this->cod_cliente,
            'cod_producto' => $this->cod_producto,
            'cantidad' => $this->cantidad,
            'base_imponible_unitaria_venta' => $this->base_imponible_unitaria_venta,
            'beneficio' => $this->beneficio,
            'fecha_de_salida' => $this->fecha_de_salida,
        ]);

        $query->andFilterWhere(['like', 'referencia_interna_venta', $this->referencia_interna_venta]);

        return $dataProvider;
    }
}

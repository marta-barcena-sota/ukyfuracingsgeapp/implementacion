<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EnlacesSeguimientos;

/**
 * EnlacesSeguimientosSearch represents the model behind the search form of `app\models\EnlacesSeguimientos`.
 */
class EnlacesSeguimientosSearch extends EnlacesSeguimientos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_enlace_seguimiento', 'codigo_seguimiento'], 'integer'],
            [['enlace_seguimiento'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EnlacesSeguimientos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_enlace_seguimiento' => $this->codigo_enlace_seguimiento,
            'codigo_seguimiento' => $this->codigo_seguimiento,
        ]);

        $query->andFilterWhere(['like', 'enlace_seguimiento', $this->enlace_seguimiento]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados_que_trabajan_en_proyectos".
 *
 * @property int $codigo_trabajo
 * @property int|null $codigo_proyecto
 * @property int|null $codigo_empleado
 * @property int|null $responsable
 *
 * @property Proyectos $codigoProyecto
 * @property Empleados $codigoEmpleado
 */
class EmpleadosQueTrabajanEnProyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados_que_trabajan_en_proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_proyecto', 'codigo_empleado', 'responsable'], 'integer'],
            [['codigo_proyecto', 'codigo_empleado'], 'unique', 'targetAttribute' => ['codigo_proyecto', 'codigo_empleado']],
            [['codigo_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['codigo_proyecto' => 'codigo_proyecto']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_trabajo' => 'Codigo Trabajo',
            'codigo_proyecto' => 'Codigo Proyecto',
            'codigo_empleado' => 'Codigo Empleado',
            'responsable' => 'Responsable',
        ];
    }

    /**
     * Gets query for [[CodigoProyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['codigo_proyecto' => 'codigo_proyecto']);
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }
}

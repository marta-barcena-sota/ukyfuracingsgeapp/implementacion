<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Productos;

/**
 * ProductosSearch represents the model behind the search form of `app\models\Productos`.
 */
class ProductosSearch extends Productos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_producto', 'cantidad_en_stock'], 'integer'],
            [['forma_de_almacenamiento', 'concepto_producto', 'primera_categoria_producto', 'segunda_categoria_producto', 'tercera_categoria_producto', 'referencia_interna_producto', 'referencia_articulo_producto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_producto' => $this->codigo_producto,
            'cantidad_en_stock' => $this->cantidad_en_stock,
        ]);

        $query->andFilterWhere(['like', 'forma_de_almacenamiento', $this->forma_de_almacenamiento])
            ->andFilterWhere(['like', 'concepto_producto', $this->concepto_producto])
            ->andFilterWhere(['like', 'primera_categoria_producto', $this->primera_categoria_producto])
            ->andFilterWhere(['like', 'segunda_categoria_producto', $this->segunda_categoria_producto])
            ->andFilterWhere(['like', 'tercera_categoria_producto', $this->tercera_categoria_producto])
            ->andFilterWhere(['like', 'referencia_interna_producto', $this->referencia_interna_producto])
            ->andFilterWhere(['like', 'referencia_articulo_producto', $this->referencia_articulo_producto]);

        return $dataProvider;
    }
}

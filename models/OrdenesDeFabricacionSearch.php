<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrdenesDeFabricacion;

/**
 * OrdenesDeFabricacionSearch represents the model behind the search form of `app\models\OrdenesDeFabricacion`.
 */
class OrdenesDeFabricacionSearch extends OrdenesDeFabricacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_orden_de_fabricacion', 'codigo_empleado', 'codigo_proyecto'], 'integer'],
            [['nombre_orden_de_fabricacion', 'imagen', 'estado', 'fecha_inicio_orden_de_fabricacion', 'fecha_fin_orden_de_fabricacion', 'tipo_orden_de_fabricacion', 'fecha_estimada_fin_orden_fabricacion', 'urgencia_orden_fabricacion'], 'safe'],
            [['estimacion_coste_material', 'coste_fabricacion', 'coste_transporte', 'coste_postprocesado', 'coste_maquina', 'coste_mano_de_obra', 'coste_material'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenesDeFabricacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_orden_de_fabricacion' => $this->codigo_orden_de_fabricacion,
            'codigo_empleado' => $this->codigo_empleado,
            'codigo_proyecto' => $this->codigo_proyecto,
            'fecha_inicio_orden_de_fabricacion' => $this->fecha_inicio_orden_de_fabricacion,
            'fecha_fin_orden_de_fabricacion' => $this->fecha_fin_orden_de_fabricacion,
            'fecha_estimada_fin_orden_fabricacion' => $this->fecha_estimada_fin_orden_fabricacion,
            'estimacion_coste_material' => $this->estimacion_coste_material,
            'coste_fabricacion' => $this->coste_fabricacion,
            'coste_transporte' => $this->coste_transporte,
            'coste_postprocesado' => $this->coste_postprocesado,
            'coste_maquina' => $this->coste_maquina,
            'coste_mano_de_obra' => $this->coste_mano_de_obra,
            'coste_material' => $this->coste_material,
        ]);

        $query->andFilterWhere(['like', 'nombre_orden_de_fabricacion', $this->nombre_orden_de_fabricacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'tipo_orden_de_fabricacion', $this->tipo_orden_de_fabricacion])
            ->andFilterWhere(['like', 'urgencia_orden_fabricacion', $this->urgencia_orden_fabricacion]);

        return $dataProvider;
    }
}

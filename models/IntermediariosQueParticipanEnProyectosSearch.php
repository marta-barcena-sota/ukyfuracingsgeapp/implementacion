<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IntermediariosQueParticipanEnProyectos;

/**
 * IntermediariosQueParticipanEnProyectosSearch represents the model behind the search form of `app\models\IntermediariosQueParticipanEnProyectos`.
 */
class IntermediariosQueParticipanEnProyectosSearch extends IntermediariosQueParticipanEnProyectos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_participacion', 'codigo_intermediario', 'codigo_proyecto'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntermediariosQueParticipanEnProyectos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_participacion' => $this->codigo_participacion,
            'codigo_intermediario' => $this->codigo_intermediario,
            'codigo_proyecto' => $this->codigo_proyecto,
        ]);

        return $dataProvider;
    }
}
